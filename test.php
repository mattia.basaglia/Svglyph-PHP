<?php
namespace svglyph;

require_once(dirname(__FILE__).'/hiero.php');


/// Test features added after the parser
function test_extra()
{
    
    global $styler;
    $parser = new parser(1,$styler);
    $doc = $parser->parse(<<< 'GLY'
[{-A1-}]-/-/:.-.:/-/:h/-o:O-o-O-!
t:w:t-<-t:w:t-A1->-<-A1:A2:A3->-A1:A2:A3
$r-A-b\black-d-$b-A-b\red-d\i-!
n-N35A-n:n:n-<-n:n->-!
A1-G1-!
G1-+LLATIN
PARAGRAPH!!+sG1-!
G1-!
G1-+lLATIN+s-!
+lPARA++comment+bGRAPH!!+sG1-!
G1
GLY
);
    //print_r($doc);
    echo render ( $doc, 32, $styler );
    echo "<!--\n".$parser->get_errors()."\n-->";
}

function test_parser()
{
    $parser = new parser(1);
    $doc = $parser->parse( <<< 'GLY'
.-h/-!
+g*ALFA *BETA+s-A1-+c^alfa ^beta+s-!
[&-A-&]-[{-}]-[(-)]-['-']-["-"]-[[-]]-!
A1-A1\-A1\R270-A1\r1-A1\FakeModifier\A2-A1\R270\-A1\t1-A1\50\red-A1\red\50\200\t1\\black\R90-A1\0-A\R315\500-!
++ this should not --- be rendered A1+s
A1-|te  x t<\-\\&amp;-A1-!
#b-A1&t-<-f-wA-wA->-!#b-A1&t-#e-<-f-wA-wA->-!A1&t-#b-<-f-wA-wA->-#e-!
$r-A1&t\black-<-f-wA-wA->-!$r-A1&t-$b-<-f-wA-wA->-!A1&t-$r-<-f-wA-wA->-$b-!
 %mzH__:$t=A1-p___-<//->-+lfoo+bbar+s-w&(t:t-!
//-v/-!.-v/-!..-v/-!?100-v/-!?200-v/-!
A1-+l}Foo<&amp;\+s\\+s-+bBar+s-+iBaz+s-+tbAri^iS^SS^X+s-A2-!
<H2-A1->-sw:t-bit:t-<-i-mn :n-t&w&t -anx->!n
:n:n-N35A-i-A1:f*ra-p*(t:t)*p:N1*f-!
A1:A1*A1-A1-w&zA-zA&w-.&w&zA-!
(A1:n)&t-A1&t-a##b##d-//&/-!
b##(a:n)-f#3-.:f:.#3-<S0-p*t:pt#14-S2>-
A1#31422345-A2#-t-!!
GLY
);
    //print_r($doc);
    echo render ( $doc, 32 );
    echo "<!--\n".$parser->get_errors()."\n-->";
}

function test_text()
{
    /*header('Content-type:text/plain');
    $t = new alphabet();
    $t->load_escape_file('lib/unicode_translit');
    $string = 'AabcdefghHxX--^A^a^b^c^d^e^f^g^h^H^x^X^s.';
    echo $string."\n";
    echo $t->escaped_text($string,false)."\n";
    echo $t->escaped_text($string)."\n";
    echo $t->escaped_text($string,true,true)."\n";
    echo $t->escaped_text($string,false,true)."\n";*/
    
    $page = new page();
    $line = new line;
    
    $line->elements[] = new glyph('A1');
    $line->elements[] = new text("Hello\+World!!",'l',0);
    $line->elements[] = new text("imn twt",'t');
    $line->elements[] = new add_style ( new text("anx",'t'), 'bold' );
    $line->elements[] = new glyph('anx');
    $line->elements[] = new add_style ( new text("Hello\+World!!",'l',1), 'italic' );
    $line->elements[] = new glyph('A1');
    for ( $i = 0; $i < 1; $i += .1 )
        $line->elements[] = new text("A",'l',$i);
    $line->elements[] = new glyph('A1');
    
    $line->elements[] = new over_text("over here",'l',1);
    
    $page->elements[] = new line;
    $page->elements[] = $line;
    $page->elements[] = new line;
     
    render ( $page, 32, 32 );
}

function test_modifiers()
{
    
    //echo render ( new rotate ( new glyph('N35'), 0 ), 32 );return;
    
    $rotscale = new scale(
                          new rotate(
                                      new glyph('G1'),
                                      315 ),
                          5 );
    
    $page = new page();
    
    $line = new line;
    
    $line->elements[] = new glyph('A1');
    $line->elements[] = new rotate( new glyph('A1'), 45 );
    $line->elements[] = new glyph(get_code('xA'));
    $line->elements[] = new rotate( new glyph(get_code('xA')), 45 );
    $line->elements[] = new rotate( new glyph(get_code('xA')), 90 );
    $page->elements[] = $line;
    
    $line = new line;
        
    
    $enc = new drawn_enclosure(
                new cap_smooth_start,
                new cap_building_end
            );
        $enc->content = new add_style ( new glyph(get_code('ra')), 'black' );
    
    
    $line->elements[] = $rotscale;
    
    $line->elements[] = new mirror ( new add_style ( new partial_shade ( $enc, '134' ), 'red' ) );
    
    $line->elements[] = new glyph('A1');
    
    $line->elements[] = new add_style ( new glyph('A1'), 'red' );
    
    $line->elements[] = new partial_shade ( new mirror ( new glyph('A1') ), '1' );
    
    $line->elements[] = new shade(1,1);
    
    $line->elements[] = new partial_shade ( new rotate ( new glyph('M17'), 45 ), '1234' ) ;
    
    $line->elements[] = new partial_shade ( new rotate ( new glyph('I9'), 90 ), '1234' ) ;
    
    $line->elements[] = new partial_shade ( new rotate ( new glyph('N35'), 45 ), '1234' ) ;
    
    $line->elements[] =  new rotate ( new partial_shade ( new glyph('I9'), '124' ), -45 );
    
    $line->elements[] =  new rotate ( $enc, 90 );
    
    $line->elements[] =  new rotate ( new glyph (get_code('ra')), 75 );
    $line->elements[] = new glyph (get_code('ra'));
    
    $line->elements[] = new glyph (get_code('t'));
    $line->elements[] = new scale ( new glyph (get_code('t')), 1 );
    $line->elements[] = new scale ( new glyph (get_code('t')), 2 );
    $line->elements[] = new scale ( new glyph (get_code('t')), 5 );
    $line->elements[] = new glyph ('A1');
    $line->elements[] = new scale ( new glyph ('A1'), 2 );
    $line->elements[] = new scale ( new glyph ('A1'), .5 );
    
        $sub = new subordination;
            $cat = new concatenation;
                $cat->elements[] = new glyph(get_code('p'));
                $cat->elements[] = new glyph(get_code('t'));
            $sub->elements[] = $cat;
            $sub->elements[] = new glyph(get_code('pt'));
        $line->elements[] = $sub;
        
        $sub = new subordination;
            $cat = new concatenation;
                $cat->elements[] = new scale ( new glyph (get_code('p')), 1.2 );
                $cat->elements[] = new scale ( new glyph (get_code('t')), 1.2 );
            $sub->elements[] = $cat;
            $sub->elements[] = new glyph(get_code('pt'));
        $line->elements[] = $sub;
    
    $page->elements[] = new line;
    $page->elements[] = $line;
    
        $line = new line;
        $gly = new glyph('N35');//new overlay ( new glyph('n'), new shade(1,1) );
            for ( $i = 0; $i < 360; $i+=10 )
                $line->elements[] = new partial_shade ( new rotate ( $gly, $i ), '1234' );
        $page->elements[] =  new add_style ( $line, 'red' );
        
    $page->elements[] = new line;
    
    echo render ( $page, 32 );
}

function test_line_page()
{
    $page = new page();
        $line = new line;
            $line->elements[] = new glyph(get_code('ra'));
            $line->elements[] = new glyph('A1');
        $page->elements[] = $line;
        $page->elements[] = $line;
    echo render ( $page, 32 );
    
}


function test_enclosures()
{
    $doc = new concatenation;
     
    
    
    $name = new concatenation;
        $name->elements[] = new glyph('i');
        $sub = new subordination;
            $sub->elements[] = new glyph('mn');
            $sub->elements[] = new glyph('n');
        $name->elements[] = $sub;
        $twt = new bird_ligature;
            $twt->left = new glyph('t');
            $twt->main = new glyph('w');
            $twt->right = new glyph('X1');
        $name->elements[] = $twt;
        $name->elements[] = new glyph('anx');
        
    $enc = new drawn_enclosure(new cap_hwt_bottom_start, new cap_hwt_bottom_end );
        $enc->content = $name;
    $doc->elements[] = $enc;
    
    $enc = new drawn_enclosure(new cap_hwt_top_start, new cap_hwt_top_end );
        $enc->content = $name;
    $doc->elements[] = $enc;
    
    $enc1 = new drawn_enclosure(
                new cap_knot_start,
                new cap_smooth_end
            );
        $enc1->content = new glyph('w');
    $doc->elements[] = $enc1;
    
    $doc->elements[] = new shade(.5,1);
    $doc->elements[] = new shade(.5,1);
    
    $enc = new drawn_enclosure;
        $enc->content = $name;
    $doc->elements[] = $enc;

    $doc->elements[] = $enc1;
    
    $enc = new drawn_enclosure(
                new cap_smooth_start,
                new cap_smooth_end
            );
        $enc->content = new glyph('ra');
    $doc->elements[] = $enc;
    
    $enc = new drawn_enclosure(new cap_void_start, new cap_void_end );
        $enc->content = $name;
    $doc->elements[] = $enc;
    
    $doc->elements[] = new shade(.5,1);
    
    $enc = new drawn_enclosure(new cap_square_start, new cap_square_end );
        $enc->content = $name;
    $doc->elements[] = $enc;
    
    $doc->elements[] = new shade(.5,1);
    
    $enc = new drawn_enclosure(new cap_square_start, new cap_building_end );
        $enc->content = $name;
    $doc->elements[] = $enc;
    
    $doc->elements[] = new shade(.5,1);
    
    $enc = new drawn_enclosure(
                new cap_building_start,
                new cap_smooth_end
            );
        $enc->content = new glyph('ra');
    $doc->elements[] = $enc;
    
    render ( $doc, 32, 32 );
}

function test_glyph_rendering()
{
    $doc = new concatenation;
    
    $doc->elements[] = new glyph('n');
    $doc->elements[] = new glyph('ra');
    $doc->elements[] = new glyph('t');
    
    $doc->elements[] = new glyph('A1');
    $sub = new subordination;
        $sub->elements[] = new glyph('A1');
        $sub->elements[] = new glyph('A1');
    $doc->elements[] = $sub;
    $doc->elements[] = new glyph('A1');
    $doc->elements[] = new glyph('f');
    $doc->elements[] = new glyph('n');
    
    $sub = new subordination;
        $sub->elements[] = new glyph('n');
        $sub->elements[] = new glyph('f');
        $sub->elements[] = new glyph('n');
    $doc->elements[] = $sub;
    
    $sub = new subordination;
        $sub->elements[] = new glyph('f');
        $jux = new concatenation;
            $jux->elements[] = new spacer(1,1);
            $jux->elements[] = new glyph('ra');
        $sub->elements[] = $jux;
    $doc->elements[] = $sub;
    
    $sub = new subordination;
        $sub->elements[] = new glyph('f');
        $jux = new concatenation;
            $jux->elements[] = new glyph('A1');
            $jux->elements[] = new glyph('ra');
        $sub->elements[] = $jux;
    $doc->elements[] = $sub;
    
    $ov = new overlay;
        $ov->back = new glyph('b');
        $ov->front = new glyph('a');
    $doc->elements[] = $ov;
    
    
    $twt = new bird_ligature;
        $twt->left = new glyph('t');
        $twt->main = new glyph('w');
        $twt->right = new glyph('X1');
    $doc->elements[] = $twt;
    $doc->elements[] = new glyph('t');
    
    $doc->elements[] = new glyph('w');

    $sara = new bird_ligature;
        $sara->left = new glyph('p');
        $sara->main = new glyph('zA');
        $sara->right = new glyph('ra');
    $doc->elements[] = $sara;
    $doc->elements[] = new glyph('zA');
    $doc->elements[] = new glyph('ra');
    
    $sara = new bird_ligature;
        $sara->left = new glyph('t');
        $sara->main = new glyph('D');
        $sara->right = new glyph('t');
    $doc->elements[] = $sara;
    
    $ov = new overlay;
        $ov->back = new glyph('D');
        $sub = new subordination;
            $sub->elements[] = new spacer(1,1);
            $sub->elements[] = new glyph('t');
        $ov->front = $sub;
    $doc->elements[] = $ov;
    
    
    $sub = new subordination;
        $sub->elements[] = new glyph('D');
        $sub->elements[] = new glyph('t');
    $doc->elements[] = $sub;
    
    render ( $doc, 32 );
}


/**
 * Glyph placeholder for layout testing
 */
class test_glyph extends drawable
{
    public $path = ''; ///< SVG <path d=''> attribute used to draw the glyph
    
    function __construct($ratio_w, $ratio_h)
    {
        
        parent::__construct($ratio_w, $ratio_h);
    }
    
    function render($x,$y,$height)
    {
        $h = $this->height($height);
        $w = $this->width($height);
        if ( $h < $height )
            $y += ($height-$h)/2; // place at middle
        return "<rect class='shade' x='$x' y='$y' width='$w' height='$h' />\n";
    }
}


function test_layout_hor()
{
    $doc = new concatenation;
    
    $doc->elements[] = new shade(2,.5);
    $doc->elements[] = new shade(.5,1);
    $doc->elements[] = new shade(1,.5);
    $doc->elements[] = new shade(1,1);
    
    $sub = new subordination;
        $sub->elements[] = new shade(1,1);
        $sub->elements[] = new shade(4,.25);
        $sub->elements[] = new shade(4,.25);
        $sub->elements[] = new shade(4,.25);
    $doc->elements[] = $sub;
    
    $doc->elements[] = new test_glyph(2,.5);
    $doc->elements[] = new test_glyph(.5,1);
    $doc->elements[] = new test_glyph(1,.5);
    $doc->elements[] = new test_glyph(1,1);
    
    $sub = new subordination;
        $sub->elements[] = new test_glyph(1,1);
        $sub->elements[] = new test_glyph(4,.25);
    $doc->elements[] = $sub;
    
    $sub = new subordination;
    $sub->elements[] = new test_glyph(2,.5);
        $jux = new concatenation;
            $jux->elements[] = new test_glyph(1,1);
            $jux->elements[] = new test_glyph(1,.5);
        $sub->elements[] = $jux;
    $doc->elements[] = $sub;
    $doc->elements[] = $jux;
    $sub = new subordination;
        $sub->elements[] = new test_glyph(2,.5);
        $sub->elements[] = new test_glyph(1,.5);
    $doc->elements[] = $sub;
    
    
    $sub = new subordination;
        $sub->elements[] = new test_glyph(4,.25);
        $sub->elements[] = new test_glyph(4,.25);
    $doc->elements[] = $sub;
    
    $over = new overlay;
        $over->front = new test_glyph(.5,1);
        $over->back = new test_glyph(2,.5);
    $doc->elements[] = $over;
    
    $over = new overlay;
        $over->front = new test_glyph(1,1);
        $over->back = new test_glyph(1,.5);
    $doc->elements[] = $over;
    $over = new overlay;
        $over->back = new test_glyph(.5,1);
        $over->front = new test_glyph(1,1);
    $doc->elements[] = $over;
    
    $over = new overlay;
        $over->front = new test_glyph(1,.5);
        $over->back = new test_glyph(1,.5);
    $doc->elements[] = $over;
    
    $bird = new bird_ligature;
        $bird->left = new test_glyph(2,.5);
        $bird->main = new test_glyph(1,1);
        $bird->right = new test_glyph(2,1);
    $doc->elements[] = $bird;
    
    $doc->elements[] = new test_glyph(.5,1);
    
    render ( $doc, 32 );
}

?>