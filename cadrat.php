<?php
namespace svglyph;

abstract class cadrat
{
    static $line_height=32; ///< Height of a line
    protected $separable=true; ///< Whether should be spaced in layouts
    
    /** \brief Reenders SVG
     * \param $x        X position (left)
     * \param $y        Y position (top)
     * \param $height   Available height
     */
    abstract function render($x,$y,$height);
    /**
     * \return the used width given available size
     */
    abstract function width($height);
    /**
     * \return the used height given available size
     */
    abstract function height($height);
    /**
     * \return Relative height, 1 = full line height
     */
    function ratio_h()
    {
        return $this->height(cadrat::$line_height)/cadrat::$line_height;
    }
    /**
     * \return Relative width, 1 = square, 2 = long rectangle
     */
    function ratio_w()
    {
        return $this->width(cadrat::$line_height)/$this->height(cadrat::$line_height);
    }
    
    /**
     * \param $angle angle in degrees
     * \return array with width and height of the rotated bounding box
     * \remarks useful to optimize rotation for glyphs
    */
    function bounding_box($height,$angle=0)
    {
        $w0 = $this->width($height);
        $h0 = $this->height($height);
        
        $alpha = deg2rad($angle);
        $w1 = abs ( sin($alpha)*$h0 ) + abs ( cos($alpha)*$w0 );
        $h1 = abs ( sin($alpha)*$w0 ) + abs ( cos($alpha)*$h0 );
        
        return array($w1,$h1);
    }
}

?>