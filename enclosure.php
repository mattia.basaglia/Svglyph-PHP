<?php

namespace svglyph;

require_once(dirname(__FILE__).'/drawable.php');

abstract class enclosure extends cadrat
{
    public $content;  ///< Enclosed cadrat
    
    function render($x,$y,$height)
    {
        $h = $height-2*$this->content_padding($height);
        $w = $this->content->width($h);
        
        $rendering ='<g><g>';
        $rendering .= $this->render_border($x,$y,$height,$w);
        /*$rendering .= $this->render_start_cap($x,$y,$height,$w);
        $rendering .= $this->render_border($x,$y,$height,$w);
        $rendering .= $this->render_end_cap($x,$y,$height,$w);*/
        $rendering .='</g>';
        
        $rendering .= $this->content->render(
                        $x+$this->start_cap_width($height),
                        $y+$this->content_padding($height),
                        $h
                        );
        
        return "$rendering</g>\n";
    }
    
    function height($height)
    {
        return $height;
    }
    
    function width($height)
    {
        return  $this->start_cap_width($height)+
                $this->content->width($height-2*$this->content_padding($height))+
                $this->end_cap_width($height);
    }
    
    /**
     *  \param $x position of the whole enclosure
     *  \param $y position of the whole enclosure
     *  \param $height Available height
     *  \param $width Width of contained element
    */
    abstract protected function render_border($x,$y,$height,$width);
    abstract protected function start_cap_width($height);
    abstract protected function end_cap_width($height);
    /// White space above and below the sign
    abstract protected function content_padding($height);
}

interface enclosure_cap
{
    /// \return path data for the cap
    function path_data($x,$y,$height);
    /// \return width used to push other object
    function layout_width($height);
    /// \return The actual width of what is being drawn
    function real_width($height);
}

/// Rounded start cap, like std cartouche
class cap_smooth_start implements enclosure_cap
{
    function path_data($x, $y, $height)
    {
        $x1 = $x-$height/12;
        $x2 = $x1+$height/2;
        $y1 = $y+1;
        $y2 = $y+$height-1;
        
        return "M $x2 $y1 C $x1 $y1 $x1 $y2 $x2 $y2 ";
    }
    function layout_width($height)
    {
        return $height/3-$height/12;
    }
    function real_width($height)
    {
        return $height/2-$height/12;
    }
}

/// Rounded+knot end cap, like std cartouche
class cap_knot_end implements enclosure_cap
{
    protected function raw_path_data($x1,$x2,$x3,$x4,$y,$height)
    {
        $y1 = $y+1;
        $y2 = $y+$height*5/12;
        $y3 = $y+$height/2;
        $y4 = $y+$height*7/12;
        $y5 = $y+$height-1;
        
        return  "M $x1 $y5".
                "C $x4 $y5 $x4 $y1 $x1 $y1 ". // curve
                "M $x3 $y1 L $x3 $y5 ". // stroke
                "M $x2 $y2 L $x4 $y2 ". // knot
                "M $x2 $y3 L $x4 $y3 ".
                "M $x2 $y4 L $x4 $y4 ";
    }
    function path_data($x, $y, $height)
    {
        /*
        *               y
        *               1
        * -----.,  |
        *      __:_|_   2 
        *      ___.|_   3    
        *      ___:|_ 
        *        . |    4
        * -----'`  |    5
        *                          
        * x    1 2 3  4
        */
        $x1 = $x;
        $x2 = $x1+$height/4;
        $x3 = $x1+$height*5/12;
        $x4 = $x1+$height/2;
        return $this->raw_path_data($x1,$x2,$x3,$x4,$y,$height);
    }
    
    
    function layout_width($height)
    {
        return $height/2-max(0,$height*3/12);
    }
    
    function real_width($height)
    {
        return $height*5/12;
    }
}

/// Empty cap, with pushing
class cap_void_start implements enclosure_cap
{
    function path_data($x, $y, $height)
    {
        return '';
    }
    function layout_width($height)
    {
        return $height/6;
    }
    function real_width($height)
    {
        return 0;
    }
}

/// Empty cap, without pushing
class cap_void_end implements enclosure_cap
{
    function path_data($x, $y, $height)
    {
        return '';
    }
    function layout_width($height)
    {
        return 0;
    }
    function real_width($height)
    {
        return 0;
    }
}

/// reverse of cap_knot_end
class cap_knot_start extends cap_knot_end
{
    function path_data($x, $y, $height)
    {
        $x1 = $x+$height/2;
        $x2 = $x1-$height/4;
        $x3 = $x1-$height*5/12;
        $x4 = $x;
        
        return $this->raw_path_data($x1,$x2,$x3,$x4,$y,$height);
    }
    function layout_width($height)
    {
        return $height/3;
    }
}

/// reverse of cap_smooth_start
class cap_smooth_end extends cap_smooth_start
{
    function path_data($x, $y, $height)
    {
        $x2 = $x;
        $x1 = $x2+$height/2;
        $y1 = $y+1;
        $y2 = $y+$height-1;
        
        return "M $x2 $y1 C $x1 $y1 $x1 $y2 $x2 $y2 ";
    }
}


/// Square start cap, like serekh
class cap_square_start extends cap_void_start
{
    function path_data($x, $y, $height)
    {
        $y1 = $y+1;
        $y2 = $y+$height-1;
        return "M $x $y1 L $x $y2 ";
    }
}

/// Square end cap
class cap_square_end extends cap_void_end
{
    function path_data($x, $y, $height)
    {
        $y1 = $y+1;
        $y2 = $y+$height-1;
        return "M $x $y1 L $x $y2 ";
    }
}

/// Building end cap, like serekh
class cap_building_end implements enclosure_cap
{
    protected function raw_path_data($x1,$x2,$x3,$x4,$y,$height)
    {
        $y1 = $y+1;
        $y2 = $y+$height-1;
        
        $path = "M $x4 $y1 L $x1 $y1 $x1 $y2 $x4 $y2 "; // top, bottom and side strokes
        
        $n_stokes = 8; // number of internal strokes
        for ( $i = 1; $i < $n_stokes; $i++ )
        {
            $ty = $y+$i*$height/$n_stokes;
            $tx = $i % 2 ? $x3 : $x2;
            $path .= "M $tx $ty L $x4 $ty";
        }
        
        return $path;
        
    }
    /* ___________
     *    |    ___
     *    |  _____
     *    |    ___
     *    |  _____
     *    |    ___
     * ___|_______
     *
     * x  1  2  3 4 
     */
    function path_data($x, $y, $height)
    {
        
        $x1 = $x;
        $x2 = $x+$height/5;
        $x3 = $x+$height/3;
        $x4 = $x+$height*2/3;
        
        return $this->raw_path_data($x1,$x2,$x3,$x4,$y,$height);
    }
    
    
    function layout_width($height)
    {
        return $height*2/3;
    }
    
    function real_width($height)
    {
        return $this->layout_width($height);
    }
}

/// Reverse of cap_building_end
class cap_building_start extends cap_building_end
{
    
    function path_data($x, $y, $height)
    {
        
        $x4 = $x;
        $x1 = $x+$height*2/3;
        $x3 = $x1-$height/5;
        $x2 = $x1-$height/3;
        
        return $this->raw_path_data($x1,$x2,$x3,$x4,$y,$height);
    }
    
    
    function layout_width($height)
    {
        return $height*5/6;
    }
    
    function real_width($height)
    {
        return $height*2/3;
    }
}


/// Little square at bottom
class cap_hwt_bottom_start extends cap_void_start
{
    protected function raw_path_data($x1,$x2,$y,$height)
    {
        $w = $this->real_width($height);
        $y1 = $y+1;
        $y3 = $y+$height-1;
        $y2 = $y3-$w;
        return "M $x2 $y1 L $x1,$y1 $x1,$y3 $x2,$y3 $x2,$y2 $x1,$y2";
    }
    /**
     *   ___  1
     *  |
     *  |
     *  |___  2
     *  |   |
     *  |___| 3
     *  1   2
     */
    function path_data($x, $y, $height)
    {
        $w = $this->real_width($height);
        return $this->raw_path_data($x,$x+$w,$y,$height);
    }
    
    function layout_width($height)
    {
        return $this->real_width($height)+$height/6;
    }
    
    function real_width($height)
    {
        return $height/3;
    }
    
}

/// Little square at top
class cap_hwt_top_start extends cap_void_start
{
    protected function raw_path_data($x1,$x2,$y,$height)
    {
        $w = $this->real_width($height);
        $y1 = $y+1;
        $y2 = $y1+$w;
        $y3 = $y+$height-1;
        return "M $x1 $y2 L $x2,$y2 $x2,$y1 $x1,$y1 $x1,$y3 $x2,$y3";
    }
    /**
     *   ___  1
     *  |   |
     *  |___|
     *  |     2
     *  |    
     *  |___  3
     *  1   2
     */
    function path_data($x, $y, $height)
    {
        $w = $this->real_width($height);
        return $this->raw_path_data ( $x, $x+$w, $y, $height );
    }
    
    function layout_width($height)
    {
        return $this->real_width($height)+$height/6;
    }
    
    function real_width($height)
    {
        return $height/3;
    }
    
}

/// Little square at bottom
class cap_hwt_bottom_end extends cap_hwt_bottom_start
{
    function path_data($x, $y, $height)
    {
        $w = $this->real_width($height);
        return $this->raw_path_data($x+$w, $x, $y, $height );
    }
    
    function layout_width($height)
    {
        return $this->real_width($height);
    }
    
}

/// Little square at top
class cap_hwt_top_end extends cap_hwt_top_start
{
    function path_data($x, $y, $height)
    {
        $w = $this->real_width($height);
        return $this->raw_path_data($x+$w, $x, $y, $height );
    }
    
    function layout_width($height)
    {
        return $this->real_width($height);
    }
    
}

/**
 * Visible enclosure with assigned caps and, 1px margin and 4px padding
 */
class drawn_enclosure extends enclosure
{
    protected $start_cap, $end_cap;
    
    function __construct( enclosure_cap $start_cap = null,
                          enclosure_cap $end_cap = null,
                          $content = null )
    {
        if ( isset($start_cap) )
            $this->start_cap  = $start_cap;
        else
            $this->start_cap = new cap_smooth_start;
            
        if ( isset($end_cap) )
            $this->end_cap = $end_cap;
        else
            $this->end_cap = new cap_knot_end;
            
        $this->content = $content;
    }
    
    /**                       y
     *     _____________..... 1
     *    /             \|
     *   |               |
     *   .\_____________/|... 2
     *   . .           . .
     * x 1 2           3 4
     */
    function render_border($x,$y,$height,$width)
    {
        $x1 = $x;
        $x2 = $x1+$this->start_cap->real_width($height);
        $x3 =   $x1+
                $width+
                $this->start_cap->layout_width($height)+
                $this->end_cap->layout_width($height)-
                $this->end_cap->real_width($height);
        $x4 = $x3+$this->end_cap->real_width($height);
        
        $y1 = $y+1;
        $y2 = $y+$height-1;
        
        return "<path class='enclosure' d='".
                "M $x3 $y2 L $x2 $y2 ".
                $this->start_cap->path_data($x, $y, $height ).
                $this->end_cap->path_data($x3, $y, $height).
                "M $x2 $y1 L $x3 $y1 ".
                "' />\n";
    }
    
    function content_padding($height)
    {
        return $height/8;
    }
    
    function start_cap_width($height)
    {
        return $this->start_cap->layout_width($height);
    }
    
    function end_cap_width($height)
    {
        return $this->end_cap->layout_width($height);
    }
    
}



?>