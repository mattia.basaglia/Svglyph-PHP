<?php
namespace svglyph;

require_once(dirname(__FILE__).'/cadrat.php');

/**
 * \brief Any drawable object
 */
abstract class drawable extends cadrat
{
    
    protected $ratio_w = 1, ///< default_width = height() * $ratio_w
              $ratio_h = 1; ///< default_height = cadrat::$line_height * $ratio_h
            
    function __construct( $ratio_w, $ratio_h)
    {
        $this->ratio_w = $ratio_w;
        $this->ratio_h = $ratio_h;
    }
    
    function ratio_w()
    {
        return $this->ratio_w;
    }
    
    function ratio_h()
    {
        return $this->ratio_h;
    }
    
    function is_tall()
    {
        return $this->ratio_h == 1;
    }
    
    function is_wide()
    {
        return $this->ratio_w > .5;
    }
     
    function is_large()
    {
        return $this->is_tall() && $this->is_wide();
    }
    
    function width($height)
    {
        return $this->height($height)*$this->ratio_w;
    }
    
    function height($height)
    {
        if ( $this->is_tall() || $height <= cadrat::$line_height*$this->ratio_h  )
            return $height;
        else
            return cadrat::$line_height*$this->ratio_h;
    }
}

/// stand-alone dark areas
class shade extends drawable
{
    function __construct($ratio_w, $ratio_h)
    {
        parent::__construct($ratio_w, $ratio_h);
    }
    
    function render($x,$y,$height)
    {
        $h = $this->height($height);
        if ( $h < $height )
            $y += ($height-$h)/2; // place at middle
        return "<rect class='shade' x='$x' y='$y' width='".
        $this->width($height)."' height='$h' />\n";
    }
}
/// Self explaining
class circle extends drawable
{
    public $css_class;
    
    function __construct($ratio_w, $ratio_h, $css_class='glyph')
    {
        parent::__construct($ratio_w, $ratio_h);
        $this->css_class =$css_class;
    }
    
    function render($x,$y,$height)
    {
        $h = $this->height($height)/2;
        $w = $this->width($height)/2;
        $y += $h;
        if ( $height > 2*$h )
            $y += $height/2-$h;
        $x += $w;
        return "<ellipse class='{$this->css_class}' cx='$x' cy='$y' rx='$w' ry='$h' />\n";
    }
}
/// nothing rendered, used to move other objects
class spacer extends drawable
{
    function __construct($ratio_w, $ratio_h)
    {
        parent::__construct($ratio_w, $ratio_h);
    }
    
    function render($x,$y,$height)
    {
        return '';
    }
}

?>