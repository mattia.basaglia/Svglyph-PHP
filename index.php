<?php
namespace svglyph;
//phpinfo();

header ('Content-type: image/svg+xml');

require_once(dirname(__FILE__).'/hiero.php');
globals::initialize(true);
load_glyphs('lib/list.txt','lib/img');
load_transliteration('lib/translit.list');
load_size_info('lib/small.list');
text::load_default_alphabet();
text::load_alphabet('t','lib/translit.alphabet');
text::load_alphabet('g','lib/greek.beta-code.alphabet');
text::load_alphabet('c','lib/coptic.alphabet');
text::load_alphabet('r','lib/cyrillic.gost-13052.alphabet');
$styler = new styler;
$styler->register_all('i',"fill-opacity:.2;stroke-opacity:.2;");
$styler->register_modifier('red','red');
$styler->register_modifier('black','black');
globals::$special_glyphs['o'] = new circle(1,.5,'black glyph');
globals::$special_glyphs['O'] = new circle(1,.5,'red glyph');

include_once('test.php');

if ( ! isset ( $_SERVER['SERVER_NAME'] ) ) // only from cmd line execution of the script
{
    ini_set('xdebug.trace_format',0); // human readable output
    ini_set('xdebug.collect_params',4); // full
    ini_set('xdebug.collect_return',1);
    ini_set('xdebug.auto_trace',1);
    ini_set('xdebug.var_display_max_data',64);
    xdebug_enable();
    xdebug_start_trace('trace.xt');
}
//test_layout_hor();
//test_glyph_rendering();
//test_enclosures();
//test_line_page();
//test_modifiers();
//test_text();
//test_parser();
//test_extra();

$parser = new parser(0,$styler);
$doc = $parser->parse ( file_get_contents('self.gly') );
echo render ( $doc, 32, $styler );


if ( ! isset ( $_SERVER['SERVER_NAME'] ) )
    xdebug_stop_trace();
/**
    \file 
    \todo make globals object something passed to functions and put there every static/global
    \todo Move styler in there as well
    \todo use it for right-to-left rendering ( add <g> scaled -1 x )
    \todo \\regex modifier -> modifier class -> \S45 (skew) + convert others
    \todo optional drawn lines between lines
    \todo vertical layout
    \todo alphabets ( (h)hebrew )
    \todo raw glyph bounding box
    \todo enclosure caps closing paths / fill insted of border
    \todo better OOD
    \todo Pages -!!
    \todo PHP GTK http://gtk.php.net/ http://www.kksou.com/php-gtk2/articles/insert-images-in-GtkTextView.php
*/
?>