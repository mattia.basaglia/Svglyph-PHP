<?php

namespace svglyph;

/// Used to add custom style rules to glyphs
class styler
{
    private $css_classes; ///< array where key=class name, value = css rules
    private $modifiers; ///< array where key = \modifier name, value = css class
    
    function __construct()
    {
        $css_classes = array();
        $modifiers = array();
    }
    
    /// \return a valid CSS class name from a given string
    static function css_class($string)
    {
        return preg_replace(
                array ( '/(-|\s)+/', '/[^[:alnum:]_-]+/' ),
                array('-','_'),
                $string);
    }
    
    function register_class($class_name,$css)
    {
        $this->css_classes[self::css_class($class_name)]=trim($css);
    }
    
    /// \return Whether the registration has been successful
    function register_modifier($lexstr,$class_name)
    {
        // $lexstr must begin with a letter and be made only of letters and numbers
        if ( preg_match('/^[[:alpha:]][[:alnum:]]*$/',$lexstr) != 0 )
        {
            $this->modifiers[$lexstr] = self::css_class($class_name);
            return true;
        }
        return false;
    }
    
    /// \return CSS corresponding to $css_classes
    function get_css()
    {
        $str = '';
        foreach ( $this->css_classes as $class => $css )
            $str .= ".$class{{$css}}";
        return $str;
    }
    
    /// Like register_class() and register_modifier() combined
    function register_all($class_name,$css)
    {
        if ( $this->register_modifier($class_name,$class_name) )
        {
            $this->register_class($class_name,$css);
            return true;
        }
        return false;
    }
    
    function get_class($modifier)
    {
        if ( isset ($this->modifiers[$modifier]) )
            return $this->modifiers[$modifier];
        return null;
    }
}

?>