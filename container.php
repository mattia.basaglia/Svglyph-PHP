<?php
namespace svglyph;

/**
    \file
    \todo vertical layout
    vertical_concatenation -> a:a a-a
    vertical_juxtaposition -> a*a
*/

require_once(dirname(__FILE__).'/cadrat.php');

/// (horizontal) a-a a*a
class concatenation extends cadrat
{
    public $elements;
    public $spacing=5;///<Distance between cadrats
    
            
    function __construct($elements=array()) 
    {
        $this->elements = $elements;
    }
    
    function render($x,$y,$height)
    {
        $rendering = '<g>';
        foreach ( $this->elements as &$e )
        {
            $rendering .= $e->render($x,$y,$height);
            $tw = $e->width($height);
            if ( $tw < 0)
            { /// negative width will move from the absolute left margin
                $tw = -$tw;
                $x = 0;
            }
            $x += $tw;
            if ( $e->separable )
                $x += $this->spacing;
        }
        return "$rendering</g>\n";
    }
    function width($height)
    {
        $w = 0;
        foreach ( $this->elements as &$e )
        {
            $tw = $e->width($height);
            if ( $tw < 0)
            { 
                $tw = -$tw;
                $w = 0;
            }
            $w += $tw;
            if ( $e->separable )
                $w += $this->spacing;
        }
        if ( $e->separable )
            $w -= $this->spacing; // remove extra spacing
        return $w;
    }
    function height($height)
    {
        $h = 0;
        foreach ( $this->elements as &$e )
        {
            $th = $e->height($height);
            if ( $th > $h )
                $h = $th;
        }
        return $h;
    }
    
    /*function ratio_w()
    {
        $w = 0;
        foreach ( $this->elements as &$e )
            $w += $e->ratio_w();
        return $w;
    }*/
}

/// (horizontal) a:a
class subordination extends cadrat 
{
    public $elements; ///< array of drawable
    
    function __construct($elements=array()) 
    {
        $this->elements = $elements;
    }
    
    /// evaluate total height (unscaled)
    private function total_height($height)
    {
        $h = 0;
        foreach ( $this->elements as &$e )
            $h += $e->height($height);
        return $h;
    }
    
    /// \return real element height
    private function scale_local_height($available_h, $total_h, $element)
    {
        return $available_h*$element->height($available_h)/$total_h;
    }
    
    function render($x,$y,$height)
    {
        if ( count($this->elements) == 0 )
            return '';
        
        $h = $this->total_height($height);
        
        $max_w = 0;
        $local_size = array();
        foreach ( $this->elements as &$e ) // evaluate local sizes
        {
            $loc_h = $this->scale_local_height($height,$h,$e);
            $loc_w = $e->width($loc_h);
            
            if ( $loc_w > $max_w )
                $max_w = $loc_w;

            $local_size[] = array('w'=>$loc_w,'h'=>$loc_h);
        }
        
        $rendering = '<g>';
        foreach ( $this->elements as &$e )
        {
            $ls = array_shift($local_size);
            $loc_h = $ls['h'];
            $loc_w = $ls['w'];
            $loc_x = $x;
            
            if ( $max_w > $loc_w )
                $loc_x += ($max_w-$loc_w)/2; // center small signs
                
            $rendering .= $e->render($loc_x,$y,$loc_h);
            $y += $loc_h; 
        }
        return "$rendering</g>\n";
    }
    function width($height)
    {
        $h = $this->total_height($height);
        $w = 0;
        foreach ( $this->elements as &$e )
        {
            $tw = $e->width($this->scale_local_height($height,$h,$e));
            if ( $tw > $w )
                $w = $tw;
        }
        return $w;
    }
    function height($height)
    {
        return $height;
    }
}

/// a##a
class overlay extends cadrat
{
    public $back, ///<Background drawable
           $front; ///< Foreground drawable
           
    function __construct ( $back = null, $front = null )
    {
        $this->back = $back;
        $this->front = $front;
    }
    
    function width($height)
    {
        return max($this->back->width($height),$this->front->width($height));
    }
    
    function height($height)
    {
        return max($this->back->height($height),$this->front->height($height));
    }
    
    function render($x,$y,$height)
    {
        // $back and $front may be ligatures, limit function calls using locals
        $back_w = $this->back->width($height);
        $front_w = $this->front->width($height);
        $backx = $frontx = $x;
        
        if ( $back_w > $front_w )
            $frontx += ($back_w-$front_w)/2;
        else if ( $back_w < $front_w )
            $backx += ($front_w-$back_w)/2;
            
        return $this->back->render($backx,$y,$height) .
            $this->front->render($frontx,$y,$height);
    }
}

// old fixed placement
/*define('LEFT_HEIGHT',1/3);
define('RIGHT_HEIGHT',1/3);
define('LEFT_V_POS',7/12);
define('LEFT_H_POS',3/12);
define('RIGHT_H_POS',2/3);*/

/// a&a&a
/**
 * Places $left and $right in $main->zone1 and $main->zone2
 */
class bird_ligature extends cadrat
{
    public $left, ///< Glyph at middle-left
           $main, ///< Large glyph at center
           $right;///< Glyph at top-right
           
        
    function __construct($left=null,$main=null,$right=null) 
    {
        $this->main = $main;
        $this->left = $left;
        $this->right = $right;
    }
           
    function render($x,$y,$height)
    {
        $rendering = '<g>';
        
        $big_w = $this->main->width($height);
        
        if ( isset($this->left) && isset($this->main->zone1) )
        {
            
            $small_height = $height*$this->main->zone1['h'];
            
            $left_w = $this->left->width($small_height);
            $rendering .= $this->left->render($x,
                                              $y+$height*$this->main->zone1['y'],
                                              $small_height);
            
            $left_h_pos = $this->main->zone1['x']+$this->main->zone1['w']; // right side of left zone
            if ( $left_w > $big_w*$left_h_pos )
                $x += $left_w - $big_w*$left_h_pos; // push cadrat if first gliph is too wide
        }
        
        $rendering .= $this->main->render($x,$y,$height);
        
        if ( isset($this->right) && isset($this->main->zone2) )
        {
            $rendering .= $this->right->render($x+$big_w*$this->main->zone2['x'],
                                               $y,
                                               $height*$this->main->zone2['h']);
        }
        
        return "$rendering</g>\n";
    }
    
    function width($height)
    {
        $big_w = $this->main->width($height);
        
        $total =  $big_w;
        
        if ( isset($this->left) && isset($this->main->zone1) )
        {
            
            $t = $this->left->width($height*$this->main->zone1['h'])
                - $big_w*($this->main->zone1['x']+$this->main->zone1['w']);
            if ( $t > 0 )
                $total += $t;
        }
            
        if ( isset($this->right) && isset($this->main->zone2) )
        {
            $t = $this->right->width($height*$this->main->zone2['h'])
                - $big_w*(1-$this->main->zone2['x']);
            if ( $t > 0 )
                $total += $t;
        }
        
        return $total;
    }
    
    function height($height)
    {
        return $this->main->height($height);
    }
    
}


/// a-!
class line extends concatenation
{
    function height($height)
    {
        return $height; // allow line scaling?
    }
}


/// Draws a line along the full document width, unused
class line_separator extends cadrat
{
    function width($height)
    {
        return PHP_INT_MAX / $height;
    }
    function height($height)
    {
        return 0;
    }
    function render($x,$y,$height)
    {
        return "<line x1='0' y1='$y' y2='$y' x2='100%' />";
    }
}

/// a-!!
class page extends cadrat
{
    public  $line_spacing = 5,
            $elements; /// array of lines
            
    function __construct($elements=array()) /// \todo give to other containers as well
    {
        $this->elements = $elements;
    }

    function height($height)
    {
        //return count($this->elements)*($height+$this->line_spacing);
        $h = 0;
        foreach ( $this->elements as &$e )
        {
            $h += $e->height($height) + $this->line_spacing;
        }
        return $h;
    }
    
    function width($height)
    {
        $w = 0;
        foreach ( $this->elements as &$e )
        {
            $tw = $e->width($height);
            if ( $tw > $w )
                $w = $tw;
        }
        return $w;
    }
    
    function render($x,$y,$height)
    {
        $rendering = '<g>';
        //$line_sep = new line_separator;
        foreach($this->elements as &$e)
        {
            $rendering .= $e->render($x,$y,$height);
            //$rendering .= $line_sep->render($x,$y-$this->line_spacing/2,$height);
            $y+=$e->height($height)+$this->line_spacing;
        }
        return "$rendering</g>";
    }
}

?>