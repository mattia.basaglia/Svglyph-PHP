++
SVGlyph Demonstration file
+s
+bSVGlyph self explanation+s-!
+iSVGlyph+s-+l is a software library that can be used to read text encoded as+s-!
+ldescribed in the Manuel de Codage (Mdc) (with some differences) and render it as
Scalable Vector Graphics (SVG) +s-!
+bHieroglyphs+s-!
+lIf configured correctly, +iSVGlyph+l can render hieroglyps given their
Cardiner codes:+s-!
?200-+lA1+s-!?200-A1-!
+lIf sign variants are properly defined they can be displayed as well+s-!
?200-+lN35A+s-!?200-N35A-!
+lThe MdC definition for transliteration is the following:+s-!
?200-+lA+s-?100-+tA+s-!
?200-+li+s-?100-+ti+s-!
?200-+la+s-?100-+ta+s-!
?200-+lw+s-?100-+tw+s-!
?200-+lb+s-?100-+tb+s-!
?200-+lp+s-?100-+tp+s-!
?200-+lf+s-?100-+tf+s-!
?200-+lm+s-?100-+tm+s-!
?200-+ln+s-?100-+tn+s-!
?200-+lr+s-?100-+tr+s-!
?200-+ll+s-?100-+tl+s-!
?200-+lh+s-?100-+th+s-!
?200-+lH+s-?100-+tH+s-!
?200-+lx+s-?100-+tx+s-!
?200-+lX+s-?100-+tX+s-!
?200-+lz+s-?100-+tz+s-!
?200-+ls+s-?100-+ts+s-!
?200-+lS+s-?100-+tS+s-!
?200-+lq+s-?100-+tq+s-!
?200-+lk+s-?100-+tk+s-!
?200-+lg+s-?100-+tg+s-!
?200-+lt+s-?100-+tt+s-!
?200-+lT+s-?100-+tT+s-!
?200-+ld+s-?100-+td+s-!
?200-+lD+s-?100-+tD+s-!
+lIt can be used to render glyph givn their transliteration+s-!
?200-+lanx+s-!?200-anx-!
+lCommon variants for signs with the same transliteration can be defined as follows:+s-!
?200-+lw+s-?100-w-!
?200-+lW+s-?100-W-!
+lUnrecognised codes will be rendered as text (see below how to properly add text)+s-!
?200-+lA1-Foo-A+s-!
?200-A1-Foo-A-!
+lSome special glyphs (punctuation etc) can be defined with customized codes:+s-!
?200-+l.:O-o-O-o:.+s-!
?200-.:O-o-O-o:.-!
+bArranging glyphs+s-!
+lConcatenation is done using the "-" operator:+s-!
?200-+lwnm-m-A2+s-!
?200-wnm-m-A2-!
+lConcatenation is done using the ":" operator:+s-!
?200-+lp:n+s-!
?200-p:n-!
+lJuxtaposition is done using the "*" operator:+s-!
?200-+lp*t:pt+s-!
?200-p*t:pt-!
+lSign can be placed over other signs using the "##" operator:+s-!
?200-+la##b+s-!
?200-a##b-!
+lSpecial ligatures can be obtained with the "&" operator+s-!
?200-+lt&w&t+s-!
?200-t&w&t-!
+lTo override the default definition of such ligatures, they can be combined with
spacing blocks (see below):+s-!
?200-+lw&zA&.+s-!
?200-w&zA&.-!
?200-+l.&w&zA+s-!
?200-.&w&zA-!
+lAll the above operators can be combined using parentheses:+s-!
?200-+l(i*(p&w&(t:t)):n*t)##f+s-!
?200-(i*(p&w&(t:t)):n*t)##f-!
+bRed areas+s-!
+lThe default color is black, but an area delimited by $r and $b will be rendered as red+s-!
?200-+lG1-G2-G3-$r-G4-G5-G6-$b-G7-G8+s-!
?200-G1-G2-G3-$r-G4-G5-G6-$b-G7-G8-!
+bSign modifiers+s-!
+lGlyphs can be mirrored adding a backslash (\) after them:+s-!
?200-+lA1-A1\\+s-!
?200-A1-A1\-!
+lThey can be rotated by 90° steps adding (\rN) after them:+s-!
?200-+lA1-A1\r1-A1\r2-A1\r3-A1\r4+s-!
?200-A1-A1\r1-A1\r2-A1\r3-A1\r4-!
+lThe \tN modifier will both mirror and rotate the glyphs:+s-!
?200-+lA1-A1\t1-A1\t2-A1\t3-A1\t4+s-!
?200-A1-A1\t1-A1\t2-A1\t3-A1\t4-!
+lTo rotate by any angle, the \R<angle> modifier is used:+s-!
?200-+lA1-A1\R45+s-!
?200-A1-A1\R45-!
+lGlyphs can be scaled with the \<number> modifier+s-!
+lScaled signs will never grow taller than the line+s-!
?200-+lA1-A1\50-A1\200-ra-ra\150-ra\800-ra\3200+s-!
?200-A1-A1\50-A1\200-ra-ra\150-ra\800-ra\3200-!
+lColor of a single glyph can be changed using \red or \black:+s-!
?200-+lA1-A1\red+s-!
?200-A1-A1\red-!
?200-+l$r-A1\black-A1-$b+s-!
?200-$r-A1\black-A1-$b-!
+lSVGlyph can be configure to use customized styles as well:+s-!
?200-+lA1\i-A1+s-!
?200-A1\i-A1-$b-!
+lAny unrecognised modifier will be ignored:+s-!
?200-+lA1\FooBar+s-!
?200-A1\FooBar-!
+lThese modifiers can be combined+s-!
?200-+lA1\r90\50\red+s-!
?200-A1\R90\50\red-!
+bSpacing+s-!
+lHalf-cadrat and full-cadrat spaces are encoded as "." and ".." respectively+s-!
?200-+lA1-.-A1-..-A1+s-!
?200-A1-.-A1-..-A1-!
+lTabulation ( spacing from the page left margin ) is obtained with "?<cadrat-percentage>"+s-!
?200-+lA1-?50-A1-?100-A1-?200-A1+s-!
?200-A1-?50-A1-?100-A1-?200-A1-!
+LA new line is obtained with -! and a new page with -!!
(currently SVGlyph can render only one page)+s
+bShading+s-!
+lStand-alone shading can be obtained in the following way:+s-!
?200-+lFull cadrat: //+s-.-//-!
?200-+lVertical: v/+s-.-v/-!
?200-+lHorizontal: h/+s-.-h/-!
?200-+lQuarter: /+s-.-/-!
+lTo shade parts of rendered carats use #<quarters> +s-!
+lwhere <quarters> is any string made of '1', '2', '3', '4'+s-!
?200-+lA1#1-A1#2-A1#3-A1#4+s-!
?200-A1#1-A1#2-A1#3-A1#4-!
+lThis can be applied to full cadrats, not only to glyph:+s-!
?200-+li-mn:n#14+s-!
?200-i-mn:n#14-!
+bGrammatical annotation+s-!
+lAccording to the MdC, a grammatical ralation is expressed with a "=" in front of the glyph+s-!
+lAnd word/sentence ending can be expressed with single or double space+s-!
+lSVGlyphs allows spaces to be anywhere between tokens and allows "_" and "__" to be used instead +s-!
+lAll this is ignored and rendered as normal+s-!
?200-+lwnm-m-A2-=f_+s-!
?200-wnm-m-A2-=f_-!
+bCartouches and other enclosures+s-!
+lA plain cartouche is defined with hieroglyphic data enclosed in -<- and ->-+s-!
?200-+l<-i-mn:n-t&w&t-anx->+s-!
?200-<-i-mn:n-t&w&t-anx->-!
+lSereckh and Hwt have a starting <S and <H (not case-sensitive)+s-!
?200-+l<S-i-mn:n-t&w&t-anx->+s-!
?200-<S-i-mn:n-t&w&t-anx->-!
?200-+l<H-i-mn:n-t&w&t-anx->+s-!
?200-<H-i-mn:n-t&w&t-anx->-!
+lTo select customize start/end cap for a cartouche a numeric digit can be added to the above codes+s-!
?200-+l<1-i-mn:n-t&w&t-anx-1>+s-!
?200-<1-i-mn:n-t&w&t-anx-1>-!
?200-+l<2-i-mn:n-t&w&t-anx-0>+s-!
?200-<2-i-mn:n-t&w&t-anx-0>-!
?200-+l<s2-i-mn:n-t&w&t-anx-h3>+s-!
?200-<s2-i-mn:n-t&w&t-anx-h3>-!
+l 0, s0 and h0 will select the empty start/end+s-!
?200-+l<h0-i-mn:n-t&w&t-anx-0>+s-!
?200-<h0-i-mn:n-t&w&t-anx-0>-!
+l s1 and h1 will select the square start/end+s-!
?200-+l<s1-i-mn:n-t&w&t-anx-h1>+s-!
?200-<s1-i-mn:n-t&w&t-anx-h1>-!
+l 2 is for the cartouche knot, 1 for the round start/end+s-!
?200-+l<2-i-mn:n-t&w&t-anx-1>+s-!
?200-<2-i-mn:n-t&w&t-anx-1>-!
+l s2 is for the Sereck tail +s-!
?200-+l<s2-i-mn:n-t&w&t-anx-s1>+s-!
?200-<s2-i-mn:n-t&w&t-anx-s1>-!
+l h2 has a square at the bottom and h3 has it at the top+s-!
?200-+l<h2-i-mn:n-t&w&t-anx-h3>+s-!
?200-<h2-i-mn:n-t&w&t-anx-h3>-!
+l If only the starting cap type is specified, the closing one will be guessed+s-!
?200-+l<2-i-mn:n-t&w&t-anx->+s-!
?200-<2-i-mn:n-t&w&t-anx->-!
?200-+l<h3-i-mn:n-t&w&t-anx->+s-!
?200-<h3-i-mn:n-t&w&t-anx->-!
+l Any unrecognised cap type will be rendered as a cartouche+s-!
?200-+l<h5-i-mn:n-t&w&t-anx-3>+s-!
?200-<h5-i-mn:n-t&w&t-anx-3>-!
+bNon-Hieroglyphic text+s-!
+lText insertions must be enclosed in \+<code> and \+s+s-!
+lWhere <code> is a lowercase letter that defines the language to be used+s-!
+lSuch codes are defined in the MdC as follows+s-!
?200-+ll = Latin text+s-!
?200-+lb = Bold Latin -+s-+bExample+s-!
?200-+ll = Italic Latin -+s-+iExample+s-!
?200-+lt = Transliteration -+s-+t^AxamplA+s-!
?200-+lg = Greek -+s-+g*PAR/ADEIUMA+s-!
?200-+lc = Coptic -+s-+c^Example+s-!
?200-+lh = Hebrew -+s-+hNot defined+s-!
?200-+lr = Cyrillic -+s-+r|KZ\MPL@Q+s-!
+lThe syntax for such alphabets (or any other) can be configured with external files+s-!
+lThe default transliteration table is shown at the beginning of this document+s-!
+lupper case transliteration is obtained prepending a caret to the character+s-!
?200-+l\+txiTA^x^i^T^A\+s+s-!
?200-+txiTA^x^i^T^A+s-!
+LThe default Greek file uses (upper-case) Beta-Code
The default Cyrillic file uses GOST 13052
The default Coptic file uses a made up map similar to the one used by transliteration
If <code> is upper-case, the following text will be rendered as a paragraph,
with newline characters breaking the lines (instead of being rendered as characters )
If <code> is equal to '\+' (\+\+) the subsequent text is parsed as a comment and
will be translated to XML comments in the final SVG
Additional escape sequences are \\\+ for \+ and \\\\ for \\+s-!
+bAnnotations+s-!
+lTo add a textual annotation use a pipe(|) follwed by text and terminated by a dash(-)+s-!
?200-+l|some text-G1+s-!
?200-|some text-G1-!
+lTo insert a - use \\- and \\\\ for a \\+s-!
+bLarge brackets+s-!
+lAdded by the publisher+s-!
?200-+l[&-i-w-m-z:H-mzH-m-i-t:r-w-N35A-&]+s-!
?200-[&-i-w-m-z:H-mzH-m-i-t:r-w-N35A-&]-!
+lRemoved by the publisher+s-!
?200-+l[{-i-w-m-z:H-mzH-m-i-t:r-w-N35A-}]+s-!
?200-[{-i-w-m-z:H-mzH-m-i-t:r-w-N35A-}]-!
+lAdded by the scribe+s-!
?200-+l['-i-w-m-z:H-mzH-m-i-t:r-w-N35A-']+s-!
?200-['-i-w-m-z:H-mzH-m-i-t:r-w-N35A-']-!
+lErased by the scribe+s-!
?200-+l[[-i-w-m-z:H-mzH-m-i-t:r-w-N35A-]]+s-!
?200-[[-i-w-m-z:H-mzH-m-i-t:r-w-N35A-]]-!
+lNo longer readable+s-!
?200-+l["-i-w-m-z:H-mzH-m-i-t:r-w-N35A-"]+s-!
?200-["-i-w-m-z:H-mzH-m-i-t:r-w-N35A-"]-!
+lMinor addition+s-!
?200-+l[(-i-w-m-z:H-mzH-m-i-t:r-w-N35A-)]+s-!
?200-[(-i-w-m-z:H-mzH-m-i-t:r-w-N35A-)]-!