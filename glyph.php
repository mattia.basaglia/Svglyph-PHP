<?php

namespace svglyph;

require_once(dirname(__FILE__).'/drawable.php');
require_once(dirname(__FILE__).'/glyph_cache.php');

abstract class glyph_base extends drawable
{
    public $zone1 = null, ///< Array xywh with the definition of left ligature zone
           $zone2 = null; ///< Array xywh with the definition of left ligature zone
    public $img_height = 1, ///< Height of the SVG image
           $img_width = 1; ///< Width of the SVG image
    protected $dx = 0, ///< Image x offset
              $dy = 0; ///< Image y offset
    protected $code; ///< Gardiner code
    
    /// copy constructor
    public function __construct($ratio_w,$ratio_h) 
    {
        parent::__construct($ratio_w,$ratio_h);
    }
    /// Removes offset present in source image
    protected function offset(&$x,&$y,$scale)
    {
        $x += $this->dx*$scale;
        $y += $this->dy*$scale;
    }
    
}

/// Cached raw_glyph
class glyph extends glyph_base
{
    
    /// \param \code Glyph code, must be a key in globals::$hiero_glyph_map
    function __construct($code)
    {
        
        if ( !isset(globals::$hiero_glyph_map[$code]) )
            throw new \Exception("glyph::__construct: '$code' is not mapped to SVG image");
            
        $this->code = $code;
        
        $copy = globals::$glyph_cache->register_glyph($this->code);
        
        // parent cctor? :-(
        $this->zone1 = $copy->zone1;
        $this->zone2 = $copy->zone2;
        $this->img_height = $copy->img_height;
        $this->img_width = $copy->img_width;
        $this->ratio_w = $copy->ratio_w;
        $this->ratio_h = $copy->ratio_h;
        $this->dx = $copy->dx;
        $this->dy = $copy->dy;
        $this->code = $copy->code;
        
    }
    
    function render($x,$y,$height)
    {
        $h = $this->height($height);
        $w = $this->width($height);
        if ( $h < $height )
            $y += ($height-$h)/2; // place at middle
            
        $scale = $h / $this->img_height;
        
        $this->offset($x,$y,$scale);
        
/*$deb ="
rh={$this->ratio_h} rw={$this->ratio_w}
ha=$height
h=$h,w=$w
iw={$this->img_width}, ih={$this->img_height}
y=$y
";*/
        return
            //"<!--$deb-->".
            /*"<rect class='shade' ".
                "x='".($x+$w*$this->zone2['x'])."' y='$y' width='".
                ($w*$this->zone2['w'])."' height='".($h*$this->zone2['h'])."' />".
            "<rect class='shade' ".
                "x='".($x+$w*$this->zone1['x'])."' y='".($y+$h*$this->zone1['y'])."' width='".
                ($w*$this->zone1['w'])."' height='".($h*$this->zone1['h'])."' />".*/
        
        "<use transform='translate($x,$y)scale($scale)' xlink:href='#{$this->code}' />\n";
    }
    
    /*function bounding_box($height,$angle=0)
    {      
        if ( !globals::$optimal_rotation )
            return parent::bounding_box($height,$angle);
        $copy = globals::$glyph_cache->register_glyph($this->code);
        if ( $copy != null )
            return $copy->bounding_box($height,$angle);
        return null;
    }*/
}

/// A glyph loaded from a file from a code as defined in globals::$hiero_glyph_map and $hiero_translit_map
class raw_glyph extends glyph_base
{
    public $path = ''; ///< SVG <path d=''> attribute used to draw the glyph
    protected $coords; ///< Array of arrays w/ coords. used iff globals::$optimal_rotation == true
    
    /*function bounding_box($height,$angle=0)
    {      
        if ( !globals::$optimal_rotation )
            return parent::bounding_box($height,$angle);
        
        //$minx = $maxx = $miny = $maxy = null;
        $alpha = deg2rad($angle);
        $cos = cos($alpha);
        $sin = sin($alpha);
        $cx = $this->img_width / 2 - $this->dx;
        $cy = $this->img_height / 2 - $this->dy;
        foreach ( $this->coords as $coord )
        {
            list($x,$y) = $coord;
            
            $dx = $x-$cx;
            $dy = $y-$cy; // correct sign?
            //$d = sqrt($dx*$dx+$dy*$dy);
            //$alpha1 = $alpha + atan2($dy,$dx);
            
            
            $x1 = $dx*$cos - $dy*$sin + $cx;
            $y1 = $dx*$sin + $dy*$cos + $cy;
            
            //echo "({$this->code},y1=$y1,y=$y,dy=$dy)\n";
            
            if ( !isset ($minx) )
            {
                $minx = $maxx = $x1;
                $miny = $maxy = $y1;
            }
            else
            {
                if ( $x1 < $minx )
                    $minx = $x1;
                if ( $x1 > $maxx )
                    $maxx = $x1;
                if ( $y1 < $miny )
                    $miny = $y1;
                if ( $y1 > $maxy )
                    $maxy = $y1;
                
            }
        }
        $h = $maxy - $miny;
        $w = $maxx - $minx;
        $scale = 1;//$h / $this->img_height;
        //echo "({$this->code},w=$w,h=$h,mx=$minx,Mx=$maxx,s=$scale,H={$this->img_height})\n";
        return array ( $w*$scale, $h*$scale );
    }*/
    
    /// parse $this->path and fill $this->img_width and $this->img_height and
    /// zone1, zone2 if they are not already set 
    function size_from_path()
    {
        
        $sep = "\s*,?\s*";
        $coordinate_regex = "(?:\+|-)?\d+(?:\.\d+)?"; /// \todo disallow -number?
        $coord_get = "($coordinate_regex$sep$coordinate_regex)$sep";
        $coord_discard = "(?:$coordinate_regex$sep$coordinate_regex)$sep";

        $move_to = "M\s*(?:$coord_get)+";
        $line_to = "L\s*(?:$coord_get)+";
        /// \note Discards (H|h) Horizontal and (V|v) Vertical lines
        $curve_to = "C\s*(?:{$coord_discard}{$coord_discard}{$coord_get})+";
        $s_curve_to = "S\s*(?:{$coord_discard}{$coord_get})+";
        $quad_to = "Q\s*(?:{$coord_get}{$coord_get})+";
        $t_quad_to = "T\s*(?:$coord_get)+";
        /// \note Discards (A|a) elliptical arch
        
        $pattern = "(?:$move_to)|(?:$line_to)|(?:$curve_to)|(?:$s_curve_to)|(?:$quad_to)|(?:$t_quad_to)";
        //$pattern = "[M|L|C|S|Q|T]\s*(?:$coord_get)+";
        $nmatch = preg_match_all("/$pattern/i",
                       $this->path, $matches );
        
        /*$minx = null;
        $miny = null;
        $maxx = null;
        $maxy = null;*/
        
        

        $this->coords = array();
        
        // Evaluate max-min coords for bounding box size
        for ( $i = 1; $i < count($matches); $i++ )
        {
            for ($j = 0; $j < $nmatch; $j++ )
            {
                if ( $matches[$i][$j] != '' )
                {
                    preg_match_all("/$coordinate_regex/",$matches[$i][$j],$coord_match);
                    
                    $tx = $coord_match[0][0];
                    if ( isset($coord_match[0][1]) )
                        $ty = $coord_match[0][1];
                    else
                        $ty = 0;
                    $this->coords[] = array($tx,$ty);

                    if ( !isset($maxx) )
                    {
                        $maxx = $tx;
                        $minx = $tx;
                        $maxy = $ty;
                        $miny = $ty;
                    }
                    else
                    {
                        if ( $tx > $maxx )
                            $maxx = $tx;
                        else if ( $tx < $minx )
                            $minx = $tx;
                            
                        if ( $ty > $maxy )
                            $maxy = $ty;
                        else if ( $ty < $miny )
                            $miny = $ty;
                    }
                }
            }
        }
        
        /*if ( !isset($minx) )
        {
            echo "Path: {$this->path}\n";
            print_r($matches);
        }*/
        
        $this->img_width = $maxx - $minx;
        $this->img_height = $maxy - $miny;
        
        // move if not centered
        $midx = ($maxx+$minx)/2;
        $midy = ($maxy+$miny)/2;
        if ( $midx != $this->img_width/2 || $midy != $this->img_height/2 )
        {
            $this->dx = $this->img_width/2 - $midx;
            $this->dy = $this->img_height/2 - $midy;
        }
        
        // compute ligature zone if not defined in svg data
        /**
         * Zone2 algorithm
         * foreach coord
         *    Evaluate if current coord is above or below diagonal /
         *      if above, choose max x
         *      if below, choose min y
         *
         * / diagonal equation ??
         * 00___w0     h
         *  |  /|      |\
         *  | / |  ->  | \
         *  |/__|      |__\_
         * 0h   wh     O  w
         * y = bx+c
         * c = h
         * b = dy/dx = -h/w
         * y = x*-h/w + h = (-x/w+1)*h
         * if y < x*-h/w + h
         *      above
         *
         * Zone1 algorithm
         * Same as zone2 with comparison line:
         *  00___w0      h  
         *   | .'|       | .'
         *h/3|/  | -> h/3|/ :
         *   |___|       |__:__
         *  0h   wh      O  w
         * y = bx+c
         * c = h/3
         * b = dy/dx = (h*2/3)/w
         * y = x*(h*2/3)/w + h/3 = (x*2/w+1)*h/3
         */
        
        $set_1 = !isset($this->zone1); // useful once initialize zones
        $set_2 = !isset($this->zone2);
        
        if (  $set_1 || $set_2  )
        {
            if ( $set_2 )
            {
                $this->zone2 = array();
                $this->zone2['y'] = $miny; // zone2 always from top
                $this->zone2['h'] = $maxy;
                $this->zone2['x'] = $minx;
                $this->zone2['w'] = $maxx;
            }
            
            if ( $set_1 )
            {
                $this->zone1 = array();
                $this->zone1['y'] = $miny;
                $this->zone1['h'] = $maxy;
                $this->zone1['x'] = 0;
                $this->zone1['w'] = $maxx;
            }    
            
            foreach ( $this->coords as $coord )
            {                   
                $tx = $coord[0];
                $ty = $coord[1];
                // zone2
                if ( $set_2 )
                {
                    $diagonal_y = (-$tx/$this->img_width + 1 ) * $this->img_height;
                    if ( $ty < $diagonal_y )
                    {
                        if ( $tx > $this->zone2['x'] )
                        {
                            $this->zone2['x'] = $tx;
                            $this->zone2['w'] = $maxx-$tx;
                        }
                    }
                    else if ( $ty < $this->zone2['h'] )
                        $this->zone2['h'] = $ty;
                }
                if ( $set_1 )
                {
                    $diagonal_x = (2*$tx/$this->img_width + 1 ) * $this->img_height/3;
                    if ( $ty > $diagonal_x )
                    {
                        if ( $tx < $this->zone1['w'] )
                            $this->zone1['w'] = $tx;
                    }
                    else if ( $ty > $this->zone1['y'] )
                        $this->zone1['y'] = $ty;
                }
                
                    }
            if ( $set_1 )
            {
                $midy_p = $minx + $this->img_height*2/3; // 2/3 height of svg image (zone1 vertical middle)
                $half_height = abs( $midy_p - $this->zone1['y'] );
                
                if ( $this->zone1['y'] > $midy_p )
                    $this->zone1['y'] = $midy_p - $half_height;
                    
                $this->zone1['y'] += $this->img_height/16; // padding
                
                $this->zone1['w'] -= $this->img_width/16; // padding
                if ( $this->zone1['w'] < 0)
                    $this->zone1['w'] = 0;
                
                $this->zone1['h'] = $half_height*2;  
                
            }
            if ( $set_2 )
            {
                $this->zone2['h'] -= $this->img_height/16; // padding
                if ( $this->zone2['h'] < 0)
                    $this->zone2['h'] = 0;
                    
                $this->zone2['x'] += $this->img_width/16; 
            }
        }
        
    }
    
    
    
    function render($x,$y,$height)
    {
        return "<path id='{$this->code}' class='glyph' d='{$this->path}' />\n";
        /// \todo function to normalize id? -> also in glyph_cache and glyph::render
    }
    
    /**
     * \brief Get path thescription
     *
     * Searches for $svg->children,
     * if one of them is a group <g> it will search recursively into that,
     * if one is a <path> it will return its d='' attribute
     * it it finds a rectangle with id 'zone1' or 'zone2' will use that for
     * bird ligartures (otherwise it will be computed by size_from_path() )
     * \param svg an object constructed with simple XML
     * \returns the concatenation of path descriptions
     * \remarks The path data should be absolute (not relative)
     */
    private function get_path_d($svg)
    {
        $ret = '';
        foreach ( $svg->children() as $v )
        {
            $t = '';
            if ( $v->getName() == 'g' )
            {
                //echo 'g';
                $t = $this->get_path_d($v);
            }
            else if ( $v->getName() == 'path' )
            {
                //echo 'path';
                $att = $v->attributes();
                $t = $v['d'];
            }
            else if ($v->getName() == 'rect' )
            {
                $att = $v->attributes();
                $id = (string)$att['id'] ;
                
                if ( $id == 'zone1' || $id == 'zone2' )
                {
                    $z =& $this->$id;
                    $z = array();
                    $z['w'] = (double)$att['width'];
                    $z['h'] = (double)$att['height'];
                    $z['x'] = (double)$att['x'];
                    $z['y'] = (double)$att['y'];
                }
            }
            
            if ( $t != '' )
                $ret .= "$t ";
        }
        return $ret;
    }
    
    /// Scales the ligature zones to relative sizes
    function normalize_zones()
    {
        $this->zone1['w'] /= $this->img_width;
        $this->zone1['x'] /= $this->img_width;
        $this->zone1['h'] /= $this->img_height;
        $this->zone1['y'] /= $this->img_height;
        
        //$this->zone1['x'] = $this->zone1['x']+$this->zone1['w']; // position of top-right corner
    
        $this->zone2['w'] /= $this->img_width;
        $this->zone2['x'] /= $this->img_width;
        $this->zone2['h'] /= $this->img_height;
        $this->zone2['y'] /= $this->img_height;
    }
    
    
    /// \param \code Glyph code, must be a key in globals::$hiero_glyph_map
    function __construct($code)
    {
        
        
        $this->code = $code;
        
        $svg = simplexml_load_file ( globals::$hiero_glyph_map [$code] );
        
        // absolute path + transformations for position
        $path = strtoupper($this->get_path_d($svg));
        

        $this->path = $path;
        //echo $path;
        $this->size_from_path();
        
        $ratio_w = $this->img_width / $this->img_height;
        
        if ( isset(globals::$hiero_small_map[$code]) )
        {
            $ratio_h = globals::$hiero_small_map[$code];
            if ( $ratio_h * $ratio_w > 1 )
                $ratio_h = 1/$ratio_w;
        }
        else if ( $ratio_w > 2)
        {
            $ratio_h = 1/$ratio_w;
        }
        else
            $ratio_h = 1;
        
        $this->normalize_zones();
        
        parent::__construct($ratio_w, $ratio_h);
            
    }
}


?>