<?php
/**
 * \file
 * returns SVG from GET or POST data
 */

/*print_r($_REQUEST);
die;*/

header ('Content-type: image/svg+xml');
require_once(__dir__.'/hiero.php');
svglyph\globals::initialize(true);
svglyph\load_glyphs(__dir__.'/lib/list.txt',__dir__.'/lib/img');
svglyph\load_transliteration(__dir__.'/lib/translit.list');
svglyph\load_size_info(__dir__.'/lib/small.list');
svglyph\text::load_default_alphabet();
svglyph\text::load_alphabet('t',__dir__.'/lib/translit.alphabet');
svglyph\text::load_alphabet('g',__dir__.'/lib/greek.beta-code.alphabet');
svglyph\text::load_alphabet('c',__dir__.'/lib/coptic.alphabet');
svglyph\text::load_alphabet('r',__dir__.'/lib/cyrillic.gost-13052.alphabet');
$styler = new svglyph\styler;
$styler->register_all('i',"fill-opacity:.2;stroke-opacity:.2;");
$styler->register_modifier('red','red');
$styler->register_modifier('black','black');
svglyph\globals::$special_glyphs['o'] = new svglyph\circle(1,.5,'black glyph');
svglyph\globals::$special_glyphs['O'] = new svglyph\circle(1,.5,'red glyph');

$mdc = isset($_REQUEST['mdc']) ? $_REQUEST['mdc'] : '';

$parser = new svglyph\parser(0,$styler);
$doc = $parser->parse ( $mdc );
echo svglyph\render ( $doc, 32, $styler );

// echo svglyph\globals::$hiero_translit_map['S'];
// echo svglyph\globals::$hiero_glyph_map['N37'];

?>
