<?php
namespace svglyph;

require_once(dirname(__FILE__).'/cadrat.php');
require_once(dirname(__FILE__).'/drawable.php');
require_once(dirname(__FILE__).'/glyph.php');
require_once(dirname(__FILE__).'/container.php');
require_once(dirname(__FILE__).'/enclosure.php');
require_once(dirname(__FILE__).'/sign_modifier.php');
require_once(dirname(__FILE__).'/text.php');
require_once(dirname(__FILE__).'/parser.php');
require_once(dirname(__FILE__).'/styler.php');

function render( cadrat $glyphs, ///< What has tobe rendered
                $cadrat_size,    ///< Line height
                styler $styler = null, ///< Extra styles
                $xml_heading = true ///< Whether is needed <?xml and DOCTYPE
                )
{

    
    cadrat::$line_height = $cadrat_size;
    
    $imgw = $glyphs->width($cadrat_size);
    $imgh = $glyphs->height($cadrat_size);
    
    
    return
    ( $xml_heading ?
<<< XML
<?xml version='1.0' encoding='utf-8'?>
<!DOCTYPE svg PUBLIC '-//W3C//DTD SVG 1.1//EN' 'http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd'>

XML
    : '' ).
<<< SVG
<svg xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink'
    width='$imgw' height='$imgh' >
<style type='text/css'>
    .enclosure{stroke-width:1;fill:none;stroke-linecap:round;}
    .glyph{stroke:none;}
    svg,.black{fill:black;stroke:black;}
    .red{fill:red;stroke:red;}
    .shade{stroke:none;fill:black;fill-opacity:0.3}
    .bold{font-weight:bold;}
    .italic{font-style:italic;}
SVG
.'
    text{font-family:monospace;stroke:none;font-size:'.text::font_size($cadrat_size).'px;}
    .over{font-size:'.over_text::font_size($cadrat_size).'px;}
    .editorial{font-size:'.editorial_text::font_size($cadrat_size).'px;}'.
    ( isset($styler) ? $styler->get_css() : '' ).'</style>'.
    globals::$glyph_cache->flush() . $glyphs->render( 0, 0, $cadrat_size) .'</svg>';
}

/// resets the glyph map;
function unset_glyps()
{
    
    globals::$hiero_glyph_map = array();
}

/**
 * \brief loads glyph map
 * \param $list_file name of a file containing the definitions as
 * \pre GlyphGardinerCode (spaces) File.svg
 * \param $image_dir Directory containing the files ( appended to the above file names)
 * \details The glyph files must contain a single path object.
 * \todo allow files with multiple SVG paths
 * \remarks Fills globals::$hiero_glyph_map
 */
function load_glyphs($list_file,$image_dir)
{
    
    $file = fopen($list_file,'r');
    if ( $file === false )
        return;
    while ( true )
    {
        $line = fgets($file);
        if ( feof($file) )
            break;
        list($name,$img_file) = sscanf($line,"%s %s");
        globals::$hiero_glyph_map[$name]="$image_dir/$img_file";
    }
}
/**
 * \brief loads transliteration map
 * \param $list_file name of a file containing the definitions as
 * \pre GlyphManuelTransliteration (spaces) GlyphGardinerCode
 * \remarks Fills globals::$hiero_translit_map
 */
function load_transliteration($list_file)
{
    
    $file = fopen($list_file,'r');
    if ( $file === false )
        return;
    while ( true )
    {
        $line = fgets($file);
        if ( feof($file) )
            break;
        list($translit,$code) = sscanf($line,"%s %s");
        globals::$hiero_translit_map[trim($translit)]=trim($code);
    }
}

/*/// \return Gardiner code from manuel transliteration as defined in globals::$hiero_translit_map
function translit($manuel)
{
    
    if ( isset(globals::$hiero_translit_map[$manuel]) )
        return globals::$hiero_translit_map[$manuel];
    return null;
}*/

/**
 * \param $manuel MdC code or transliteration for a glyph
 * \param $iterations Mex number of times that transliteration should be applied
 * \return Gardiner code as transliterated or null if not in globals::$hiero_glyph_map
 */
function get_code($manuel,$iterations=1)
{
    $code = $manuel;
    for ( $i = 0; $i <= $iterations; $i++ )
        if ( isset ( globals::$hiero_glyph_map[$code] ) )
            return $code;
        else if ( isset(globals::$hiero_translit_map[$code]) )
            $code = globals::$hiero_translit_map[$code];
        else return null;
    
    if ( isset ( globals::$hiero_glyph_map[$code] ) )
        return $code;
    else
        return null;
}

/**
 * \brief loads transliteration map
 * \param $list_file name of a file containing the definitions as
 * \pre GlyphGardinerCode (spaces) Size as in drawable::ratio_h
 * If not specified, the value is assumed as 1 for tall glyphs, .3 for wide glyps
 * \see glyph::__contruct()
 * \remarks Fills globals::$hiero_small_map
 */
function load_size_info($list_file)
{
    
    $file = fopen($list_file,'r');
    if ( $file === false )
        return;
    while ( true )
    {
        $line = fgets($file);
        if ( feof($file) )
            break;
        list($code,$ratio_h) = sscanf($line,"%s %f");
        globals::$hiero_small_map[$code]=$ratio_h;
    }
}


class globals
{
    static  $glyph_cache,       ///< Registers raw_glyph to avoid reading SVG files multiple times
            $hiero_glyph_map,   ///< Assoc. array, GardCode => SVG path data
            $hiero_small_map,   ///< Assoc. array, GardCode => relative height
            $hiero_translit_map,///< Assoc. array, MdC Translit => GardCode
            //$optimal_rotation,  ///< Whether to store path coords to make rotaion look more accurate, may cause performance loss
            $special_glyphs;    /**<  \brief Assoc. array, MdC Translit/CardCode => cadrat object
                                 * \details Used to construct special characters (eg: circles)
                                 * \remarks Checking for this is performed before transliteration
                                 */
            
    static function initialize()//($optimal_rotation=false)
    {
        self::$glyph_cache = new cache;
        self::$hiero_glyph_map = array();
        self::$hiero_small_map = array();
        self::$hiero_translit_map = array();
        //self::$optimal_rotation = $optimal_rotation;
        self::$special_glyphs = array();
    }
}

?>