<?php
namespace svglyph;

require_once(dirname(__FILE__).'/drawable.php');

class node
{
    public $c; ///< Children
    public $v; ///< Values
    public $d = 0; ///< Depth
}

class trie
{
    private $tree;
    private $pointer;
    
    function __construct()
    {
        $this->tree = new node;
        $this->pointer =& $this->tree;
    }
    
    function insert($key, $val)
    {
        $root =& $this->tree;
        for ( $i = 0; $i < strlen($key); $i++ )
        {
            if ( !isset($root->c[$key[$i]]) )
            {
                $root->c[$key[$i]] = new node;
                $root->c[$key[$i]]->d = $i+1;
            }
            $root =& $root->c[$key[$i]];
        }
        $root->v = $val;
    }
    
    function get($key)
    {
        $root =& $this->tree;
        for ( $i = 0; $i < strlen($key); $i++ )
        {
            if ( !isset($root->c[$key[$i]]) )
                return null;
            $root =& $root->c[$key[$i]];
        }
        return $root->v;
    }
    
    function delete($key, $val)
    {
        $root =& $this->tree;
        for ( $i = 0; $i < strlen($key); $i++ )
        {
            if ( !isset($root->c[$key[$i]]) )
                return;
            $root =& $root->c[$key[$i]];
        }
        $root->v = null; /// \todo remove unused nodes
    }
    
    /// resets pointer to root
    function start()
    {
        $this->pointer =& $this->tree;
    }
    
    /// moves pointer down the tree by given char
    /// \return whether it has really moved
    function move($char)
    {
        if ( isset($this->pointer->c[$char]) )
        {
            $this->pointer =& $this->pointer->c[$char];
            return true;
        }
        return false;
    }
    
    /// \return value at pointer
    function get_value()
    {
        return $this->pointer->v;
    }
    
    /// \return pointer position
    function get_depth()
    {
        return $this->pointer->d;
    }
    
}

/// Contains information about translating input strings.
/** Useful for MdC transliteration etc*/
class alphabet
{
    private $escape; ///< Trie of escape sequences
    
    function __construct()
    {
        $this->escape = new trie;
    }
    
    /// \returns $text where $escape is applied
    function escaped_text(
                          $text,   ///< Unescaped content
                          $escape_plus = false, ///< Escape \+ to + and \\ to \
                          $escapeXML = false ///< Escape & < > ' " to &amp; etc
                        )
    {
        $r = '';
    
        $this->escape->start();
        $i = 0;
        while ( true )
        {
            if ( $i < strlen($text) )
            {
                $c = $text[$i];
                if ( !$this->escape->move($c) )
                {
                    $v = $this->escape->get_value();
                    if ( $v == null )
                    {
                        $i -= $this->escape->get_depth(); // move back to beginning
                        $r .= $text[$i]; // starting char not escape sequence
                    }
                    else
                    {
                        $r .= $v; // escape sequence;
                        $i--; // move back to re-scan current char next pass
                    }
                    $this->escape->start();
                }
            }
            else
            {
                $d = $this->escape->get_depth();
                if ( $d > 0 )
                {
                    $v = $this->escape->get_value();
                    if ( $v == null )
                    {
                        $i -= $d;
                        $r .= $text[$i];
                        $this->escape->start();
                    }
                    else
                    {
                        $r .= $v;
                        break; // tail is escaped
                    }
                }
                break; // tail not escaped
            }
            $i++;
        }

        if ( $escape_plus )
        {
            $r = preg_replace ( '/\\\\([\\\\+])/', '\\1', $r ); // \\\\\\\\\\ :-P
        }
        
        if ( $escapeXML )
        {
            $r = htmlspecialchars($r,ENT_QUOTES);
        }
        
        $r = preg_replace('/([[:space:]]+)/', ' ', $r); /// normalize spaces
        
        return $r;
    }
    
    /**
     * \pram $file_name name of file containing the escape definitions
     *
     * An escape definition is on a single line defined as <Search> (space) <Replace>
     * Replacemens is done verbatim
     * You may include XML entities but beware of $escapeXML in escaped_text()
     */
    function load_escape_file($file_name) /// \todo manual insertion as well
    {
        $file = fopen($file_name,'r');
        if ( $file === false )
            return;
        while ( true )
        {
            $line = fgets($file);
            if ( feof($file) )
                break;
            list($k,$v) = sscanf($line,"%s %s");
            $this->escape->insert($k,$v);
        }
    }
    
    /// \return number of rendered character from $text after it has been escaped
    function actual_length($text)
    {
        $text = $this->escaped_text($text);
        return xml_length ( $text ) - count_diacritics($text);
    }
}

class text extends drawable
{
    public $text; ///< Unescaped content
    static $alphabets;///< array of alphabet instances
    public $alphabet; ///< Key of an element in static $alphabets for the currently selected alphabet
    public $anchor; ///< 1 = top 0.5 = middle 1 = bottom
    
    function __construct($text,$alphabet='l',$anchor=.5)
    {
        $this->text = $text;
        $this->alphabet = $alphabet;
        $this->anchor = $anchor;
        $this->separable = false;
    }
    
    /// creates empty alphabet with key 'l'
    static function load_default_alphabet()
    {
        self::$alphabets['l'] = new alphabet;
    }
    
    /// loads aplpabet defined in $file with key $name
    static function load_alphabet($name,$file)
    {
        self::$alphabets[$name] = new alphabet;
        self::$alphabets[$name]->load_escape_file($file);
    }
    
    static function font_size($height)
    {
        return $height/2;
    }
    
    function width($height)
    {
        $alpha = isset ( self::$alphabets[$this->alphabet] ) ? $this->alphabet : 'l';
        return self::font_size($height)*(10/16)*
                self::$alphabets[$alpha]->actual_length($this->text);
    }
    
    function height($height)
    {
        return self::font_size($height)*(10/16);
    }
    
    function render($x,$y,$height)
    {
        $alpha = isset ( self::$alphabets[$this->alphabet] ) ? $this->alphabet : 'l';
        $y += $height-($height-$this->height($height))*$this->anchor;
        return "<text x='$x' y='$y' textLength='".$this->width($height)."'>".
                self::$alphabets[$alpha]->escaped_text($this->text).
                "</text>\n";
    }
    
}

/// Text above glyphs
class over_text extends text
{
    
    function __construct($text,$alphabet='l')
    {
        parent::__construct ( $text, $alphabet );
    }
    
    static function font_size($height)
    {
        return $height/3;
    }
    
    function width($height)
    {
        return self::font_size($height)*(10/16)*
                self::$alphabets[$this->alphabet]->actual_length($this->text);
    }
    
    function height($height)
    {
        return self::font_size($height)*10/16 + 1;
    }
    
    function render($x,$y,$height)
    {
        $y1 = $y + $this->height($height);
        $x1 = $x+$this->width($height) / 2;
        return "<text class='over' x='$x' y='$y1' textLength='".$this->width($height)."'>".
                self::$alphabets[$this->alphabet]->escaped_text($this->text).
                "</text>\n".
                "<line x1='$x1' y1='".($y1+$this->height($height)).
                    "' x2='$x1' y2='".($y + $height ).
                    "' />";
    }
    
}

/// Text as big as the glyphs
class editorial_text extends text
{
    
    function __construct($text,$alphabet='l')
    {
        parent::__construct ( $text, $alphabet );
    }
    
    static function font_size($height)
    {
        return $height;
    }
    
    function width($height)
    {
        return self::font_size($height)*(10/16)*
                self::$alphabets[$this->alphabet]->actual_length($this->text);
    }
    
    function height($height)
    {
        return self::font_size($height)*10/16 + 1;
    }
    
    function render($x,$y,$height)
    {
        $y += $height-($height-$this->height($height))*$this->anchor;
        return "<text class='editorial' x='$x' y='$y' textLength='".$this->width($height)."'>".
                self::$alphabets[$this->alphabet]->escaped_text($this->text).
                "</text>\n";
    }
    
}

class comment extends cadrat
{
    public $text;
    function __construct($text)
    {
        $this->text = $text;
        $this->separable = false;
    }
    
    function width($height)
    {
        return 0;
    }
    
    function height($height)
    {
        return 0;
    }
    
    function render($x,$y,$height)
    {
        return "<!--".preg_replace('/-+/','-',$this->text)."-->\n";
    }
}

/**
 * \brief Check combining Unicode value
 * \param int $unicode numerical code for the character that needs to be cheched
 * \return bool Whether it is a combining character
 */
function is_combining_char($unicode)
{
    return ( 0x0300 <= $unicode && $unicode <= 0x036F ) // Combining Diacritical Marks
        || ( 0x1DC0 <= $unicode && $unicode <= 0x1DFF ) // Combining Diacritical Marks Supplement
        || ( 0x20D0 <= $unicode && $unicode <= 0x20FF ) // Combining Diacritical Marks for Symbols
        || ( 0xFE20 <= $unicode && $unicode <= 0xFE2F ) // Combining Half Marks
        ;
}
/**
 * \return The number of combining charachers in $str ascaped as XML entity
 */
function count_diacritics($str)
{
    preg_match_all ( '/&#([0-9]+);|&#x([0-9a-f]+);/i', $str, $entities );
    $count = 0;
    foreach ( $entities[1] as $dec )
        if ( is_combining_char ( (int)$dec ) )
            $count++;
    foreach ( $entities[2] as $hex )
        if ( is_combining_char ( hexdec($hex) ) )
            $count++;
    return $count;
}

/**
 * \returns The lenght that str would have if all XML entities ( &...; )
 * were expanded as a single character
 */
function xml_length($str)
{
    return preg_match_all ( '/[^&]|&[^;]+;/', $str, $null );
}