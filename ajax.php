<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<head>
    <title>AJAX hieroglyphic editor</title>
    <style type='text/css'>
        #svgdata{display:inline-block; vertical-align:top;}
    </style>
</head>
<body>
<?php ?>
<form method='post' action='get_image.php'>
    <textarea id='input' name='mdc' rows='24' cols='80' onkeyup='edited()'></textarea>
    <div id='svgdata'></div>
    <br/>
    <input type='submit' value='Get image'>
</form>

<script type="text/javascript">
//<![CDATA[
    var xmlhttp = new XMLHttpRequest();
    var ok = 1;
    var timeout;

    xmlhttp.onreadystatechange=function()
    {
        if (xmlhttp.readyState==4 && xmlhttp.status==200)
        {
            document.getElementById("svgdata").innerHTML=xmlhttp.responseText;
            //document.getElementById("svgdata").innerHTML='got...';//xmlhttp.responseText;
        }
    }
    
    function send(text)
    {
        //document.getElementById("svgdata").innerHTML = 'sent...';//document.getElementById("input").value;
        params = "mdc="+encodeURIComponent(text);
        xmlhttp.open("POST","get_image.php",true);
        xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
//         xmlhttp.setRequestHeader("Content-length", params.length);
//         xmlhttp.setRequestHeader("Connection", "close")
        xmlhttp.send(params);
    }
    
    function edited()
    {
        if ( ok == 1)
        {
            ok = 0;
            timeout = setTimeout("timer_cb()",500);
        }
    }
    
    function timer_cb()
    {
        //timeout = setTimeout("timer_cb()",500);
        ok = 1;
        text = document.getElementById("input").value;
        send(text);
    }
    
//]]>
</script>
</body>
</html>
