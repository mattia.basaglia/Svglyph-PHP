<?php
$messages = array();
$messages['en'] = array(
    'svglyphdesc' => 'Hieroglyphic renderer',
    'filefail' => 'SVG file generation failed',
    'thumbfail' => 'PNG thumbnail generation failed',
);
