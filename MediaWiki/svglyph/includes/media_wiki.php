<?php

require_once(dirname(__FILE__).'/hiero.php');

class mwSVGlyph
{
    public $styler;
    
    function __construct()
    {
        global $wgSVGlyphLib;
        svglyph\globals::initialize();
        svglyph\load_glyphs("$wgSVGlyphLib/list.txt","$wgSVGlyphLib/img");
        svglyph\load_transliteration("$wgSVGlyphLib/translit.list");
        svglyph\load_size_info("$wgSVGlyphLib/small.list");
        svglyph\text::load_default_alphabet();
        svglyph\text::load_alphabet('t',"$wgSVGlyphLib/translit.alphabet");
        svglyph\text::load_alphabet('g',"$wgSVGlyphLib/greek.beta-code.alphabet");
        svglyph\text::load_alphabet('c',"$wgSVGlyphLib/coptic.alphabet");
        svglyph\text::load_alphabet('r',"$wgSVGlyphLib/cyrillic.gost-13052.alphabet");
        $this->styler = new svglyph\styler;
        $this->styler->register_all('i',"fill-opacity:.2;stroke-opacity:.2;");
        $this->styler->register_modifier('red','red');
        $this->styler->register_modifier('black','black');
    }
    
    /// Genarate SVG image, convert it to PNG and return HTML <a>
    function render($file_name, $text, $cadrat_size=32)
    {
        global $wgTmpDirectory,$wgUploadDirectory,$wgUploadPath,$IP;
        
        $file_name = "svglyph-$file_name-".md5($text);
        //$file_name = "".md5($text).'.png';
        $temp_dir = "$wgTmpDirectory/svglyph";
        $temp_svg = "$temp_dir/$file_name.svg";
        $output_dir = "$wgUploadDirectory/svglyph";
        $output_path = "$wgUploadPath/svglyph";
        
        // check directories
        if ( !is_dir($output_dir) )
            mkdir( $output_dir, 0755 );
            
        if ( !is_dir($temp_dir) )
            mkdir( $temp_dir, 0755 );
            
        $svg_file = fopen($temp_svg,"w");
        if ( !$svg_file )
            return wfMsg('filefail'); 
        
        $parser = new svglyph\parser(0,$this->styler);
        $doc = $parser->parse ( $text );
        $svg_data = svglyph\render ( $doc, $cadrat_size, $this->styler );
        
        fwrite ( $svg_file , $svg_data );
        fclose ( $svg_file  );
        
        $file = new UnregisteredLocalFile(false,
                                          RepoGroup::singleton()->getLocalRepo(),
                                          $temp_svg,
                                          'image/svg+xml');
        
        $params = array(
            'width' => $doc->width($cadrat_size),
            'height'=> $doc->height($cadrat_size),
            'physicalWidth' => $doc->width($cadrat_size),
            'physicalHeight'=> $doc->height($cadrat_size),
        );

        $pre_time = time();
        
        $transform = $file->transform($params, File::RENDER_NOW);
        
        $check_file_name = $transform->getPath();
        if ( !is_file($check_file_name) )
            return wfMsg('thumbfail');
        if (  filemtime( $check_file_name ) < $pre_time )
        { // force thumbnail update if it has not been generated
            unlink($check_file_name);
            $transform = $file->transform($params, File::RENDER_NOW);
            if ( !is_file($check_file_name) )
                return wfMsg('thumbfail');
        }
        
        $html = $transform->toHtml();
        
        unlink($temp_svg); // remove temp file
        
        return $html;
    }

}

