<?php


if( !defined( 'MEDIAWIKI' ) ) {
        echo( "This is an extension to the MediaWiki package and cannot be run standalone.\n" );
        die( -1 );
}


$wgExtensionCredits['parserhook'][] = array(
        'path' => __FILE__,
        'name' => "svglyph",
        'descriptionmsg' => "svglyphdesc",
        'version' => 0.1,
        'author' => "Mattia Basaglia",
        //'url' => "http://dubdubdub.example.com",
);


$wgSVGlyphIncludes = dirname(__FILE__) . '/includes';
$wgSVGlyphLib = "$wgSVGlyphIncludes/lib";
$wgAutoloadClasses['mwSVGlyph'] = $wgSVGlyphIncludes.'/media_wiki.php';
$wgHooks['ParserFirstCallInit'][] = 'SVGlyphParserInit';
$wgExtensionMessagesFiles['svglyph'] = dirname( __FILE__ ) . '/svglyph.i18n.php';

function SVGlyphParserInit(Parser &$parser)
{
    $parser->setHook('svglyph','SVGlyphParserHook');
    /// \todo Media type hook as well
    return true; // continue hook processing
}

function SVGlyphParserHook ( $text, array $args, Parser $parser, PPFrame $frame )
{
    // how to expand macro arguments and no other wiki markup?
    // $text = $parser->recursiveTagParse( $text, $frame );
    
    if ( isset($args['size']) && is_numeric($args['size']) )
        $size = (int)$args['size'];
    else
        $size = 32;
    
    static $renderer = null;
    if ( $renderer == null )
        $renderer = new mwSVGlyph;
    
    return $renderer->render($parser->getTitle()->getText(),$text,$size);
}