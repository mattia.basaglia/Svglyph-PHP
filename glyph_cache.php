<?php
namespace svglyph;

class cache
{
    private $cache_array;
    
    function __construct()
    {
        $this->cache_array = array();
    }
    
    /// Loads a glyph given its code or transliteration unless it has already been registered
    function register_glyph ( $code )
    {
        
        $code = get_code($code,2);
        if ( $code == null )
        {
            throw new Exception("cache::register_glyph: '$code' is not mapped to SVG image");
        }
        
        if ( !isset($this->cache_array[$code]) )
            $this->cache_array[$code] = new raw_glyph($code);
            
        return $this->cache_array[$code];
    }
    
    /// Empties cache
    function clear()
    {
        $this->cache_array = array();
    }
    
    /// SVG output
    /// \returns SVG defs string
    /// \remarks Doesn't clear the cache
    function flush()
    {
        $defs = "<defs>\n";
        
        foreach ( $this->cache_array as $glyph )
        {
            $defs .= $glyph->render(0,0,0);
        }
        
        return "$defs</defs>";
    }
}



?>