<?php
namespace svglyph;
/**
 * \file
 * Grammar:
\code
document    : page_nt

page_nt     : line* line_nt?

page        : page_nt SEP '!!'

line_nt     : cadrat*

line        : line_nt SEP '!'


cadrat      : cadrat '-' cadrat
            | cadrat ':' cadrat
            | cadrat '*' cadrat
            | cadrat ## cadrat
            | cadrat '&' cadrat
            | cadrat '&' cadrat '&' cadrat
            | '(' cadrat ')'
            | cadrat shading
            | text-change
            | enclosure
            | standalone_shading
            | glyph
            | paren
            | spacer
            | modifier
            | #b- cadrat* -#e
            | over-text

text-change : '+' [s|t|l|b|i|+]

over-text   : '|' .* '-'

color-change: '$b'
            | '$r'

enclosure   : '<' 'S'? SEP (text|cadrat)* SEP '>'

standalone_shading: [h|v|/]? '/'

spacer      : '..'
            | '.'
            | '?' [0-9]+
            | 'O'
            | 'o'

paren       : [&
            | &]
            | [{
            | }]
            | ['
            | ']
            | [[
            | ]]
            | "]
            | ["

glyph       : '='? GARD_CODE transform* separator? ?

shading     : # 1? 2? 3? 4?

transform   : '\' [alpha]? [alnum]*

separator   : _
            | __
            | ' '
            | '  '
\endcode
*/

class parser
{
    private $source; ///< String with MdC to be parse
    private $text_len; ///< length of $source, used to optimize calls to strlen
    private $pos; ///< Current offset within $source
    private $eof; ///< bool whether is not possible to read any more data from $source
    private $lexval; ///< Value for the last lexed token
    private $la;    ///< Look Ahead token
    private $recover; ///< error recovery array of tokens ['tok'] and lexval ['val']
    public  $error_level; /**<
                           * \li 0 = nothing
                           * \li 1 = internal log
                           * \li 2 = PHP warning (trigger_error)
                           * \li 3 = exception */
    private $error_log; ///< array of string, used if $error_level == 1
    private $has_error;
    public  $styler; ///< styler object for custom glyph modifiers
    
    /// \todo merge lexval & lexstring & la ?
    
    function __construct($error_level = 1, styler $styler = null)
    {
        $this->error_level = $error_level;
        $this->error_log = array();
        $this->has_error = false;
        $this->styler = isset ( $styler ) ? $styler : new styler;
    }
    
    /// \returns Cadrat tree
    function parse($source)
    {
        $this->source = $source;
        $this->pos = -1;
        $this->eof = false;
        $this->recover = array();
        $this->error_log = array();
        $this->has_error = false;
        $this->text_len = strlen($this->source);
        
        $this->get_next();
        return $this->document();
        /// \todo string parameter 'document', 'page', 'line', 'enclosure', 'glyph'
        /// to parse and return only given section, default='document'
    }
    
    private function error($msg)
    {
        $this->has_error = true;
        if ( $this->error_level == 3 )
            throw new Exception($msg);
        else if ( $this->error_level == 2 )
            trigger_error ($msg);
        else if ( $this->error_level == 1 )
            $this->error_log .= "$msg\n";
    }
    
    /// \returns whether the parser encountered an error during last parse
    public function has_error()
    {
        return $this->has_error;
    }
    
    /// \returns $error_log
    public function get_errors()
    {
        return $this->error_log;
    }
    
    /// Extracts a character from $source
    /// \return extracted character or null
    private function get()
    {
        $this->pos++;
        if ( $this->pos >= $this->text_len )
        {
            $this->eof = true;
            return null;
        }
        return $this->source[$this->pos];
    }
    
    /// moves back the pointer
    private function unget($amount=1)
    {
        if ( $this->eof == true )
            return;
        
        $this->pos-=$amount;
        if ( $this->pos < 0 )
            $this->pos = 0;
    }
    
    /// Discards lookahead token and lexes the next one
    private function get_next()
    {
        $this->la = $this->lex();
    }
    
    /// Reads next token if lookahead == $tok, otherwise error
    private function match($tok)
    {
        if ( $this->la == $tok )
        {
            $this->get_next();
            return true;
        }
        else
        {
            $this->error ( "parser: expected '$tok', got '{$this->la}'.");
            return false;
        }
    }
    
    /// whole document
    /// \bug Unimplemented: stops at first page
    private function document()
    {
        return $this->page();
    }
    
    /// until (optional) -!!
    private function page()
    {
        $page = new page;
        while($this->la != 'EOF')
        {
            if ( $this->la != 'PRE' )
                $page->elements[] = $this->line();
            else
            {
                $alphabet = strtolower($this->lexval);
                while ( $this->la != '+' && $this->la != 'EOF' )
                {
                    $text = $this->lex_text(true);
                    $page->elements[] =
                        new line ( array( new text ( $text, $alphabet, .5 ) ) );
                }
                $this->get_next(); // +s
                if ( $this->la == '-' || $this->la == '!' )
                    $this->get_next();
            }
            if ( $this->la == '!!' )
            {
                $this->get_next();
                break;
            }   
        }
        
        return $page;
    }
    
    /// until (optional) -!
    private function line()
    {
        /*
            since a line is basically the same as concatenation,
            all cadrats not separated by - will be redered on the same line just
            as if they were.
        */
        $line = new line;
        while($this->la != 'EOF' && $this->la != '!!' && $this->la != 'PRE')
        {
            $line->elements[] = $this->cadrat_base();
            if ( $this->la == '!' ) 
            {
                $this->get_next();
                break;
            }   
        }
        return $line;
    }
    
    /// dummy, base of all cadrats operators
    private function cadrat_base()
    {
        return $this->cadrat_cat();
    }
    
    /// concatenation: a-b-c
    private function cadrat_cat()
    {
        $next = $this->cadrat_partial_shade();
        if ( $this->la == '-' )
        { 
            $list = new concatenation;
            $list->elements[] = $next;
            while ( $this->la == '-' )
            {
                $this->get_next();
                $list->elements[] = $this->cadrat_partial_shade();
            }
            return $list;
        }
        return $next;
    }
    
    private function cadrat_partial_shade()
    {
        $cadrat = $this->cadrat_sub();
        if ( $this->la == '#1234' )
        {
            $shade = $this->lexval;
            $this->get_next();
            return new partial_shade($cadrat,$shade);
        }
        return $cadrat;
    }
    
    /// subordination: a:b:c
    private function cadrat_sub()
    {
        $next = $this->cadrat_jux();
        if ( $this->la == ':' )
        {
            $list = new subordination;
            $list->elements[] = $next;
            while ( $this->la == ':' )
            {
                $this->get_next();
                $list->elements[] = $this->cadrat_jux();
            }
            return $list;
        }
        return $next;
    }
    
    /// juxtaposition: a*b*c
    private function cadrat_jux()
    {
        $next = $this->cadrat_ligature();
        if ( $this->la == '*' )
        {
            $list = new concatenation;
            $list->elements[] = $next;
            while ( $this->la == '*' )
            {
                $this->get_next();
                $list->elements[] = $this->cadrat_over();
            }
            return $list;
        }
        return $next;
    }
    
    /// bird ligature a&b&c
    private function cadrat_ligature()
    {
        $first = $this->cadrat_over();
        if ( $this->la == '&' )
        {
            $this->get_next();
            $second = $this->cadrat_over();
            if ( isset($second->zone1) )
            {
                if ( $this->la == '&' )
                {
                    $this->get_next();
                    $third = $this->cadrat_over();
                    return new bird_ligature($first,$second,$third);
                }
                if ( $first->ratio_h() < $second->ratio_h() ||
                    ( $first->ratio_h() == $second->ratio_h() &&
                      $first->ratio_w() < $second->ratio_w() ) )
                        return new bird_ligature($first,$second,null);
            }
            if (  isset($first->zone2) )
                return new bird_ligature(null,$first,$second);
            else // fall back to concatenation if ligature won't work
                return new concatenation(array($first,$second));
        }
        return $first;
    }
    
    /// overlay a##b
    private function cadrat_over()
    {
        $next = $this->cadrat_prim();
        while ( $this->la == '##' ) // change 'while' to 'if' to allow only 2 layers
        {
            $this->get_next();
            $next = new overlay($next, $this->cadrat_prim());
        }
        return $next;
    }
    
    private function cadrat_prim()
    {
        switch ( $this->la )
        {
            case "(":
                $this->get_next();
                $cadrat = $this->cadrat_base();
                $this->match(')');
                return $cadrat;
            
            case '<': // enclosure
                return $this->enclosure();
                
            case '/': // stand-alone shade
                $type = $this->lexval;
                $this->get_next();
                switch($type)
                {
                    case '/': return new shade(1,1); // full block
                    case 'v': return new shade(.5,1);// vertical
                    case 'h': return new shade(2,.5);// horizontal
                    default: return new shade(1,.5);// small
                }
                
            case '+':
                return $this->font_change();
                
            case '.': // spaces, tabs etc
                $w = $this->lexval;
                $this->get_next();
                return new spacer($w*2,.5);// low height but proper width
                
            case '=': // grammar
            case 'GLYPH': // self explaining ...
                return $this->glyph();
                
            case '$r': // color change
                $this->get_next();
                $area = $this->cadrat_base();
                $this->match('$b');
                return new add_style($area,'red');
                
            case '#b': // begin long shading
                $this->get_next();
                $area = $this->cadrat_base();
                $this->match('#e');
                return new partial_shade($area,'1234');
            
            case '[': // opening editorial brackets
                $t = $this->lexval;
                $this->get_next();
                if ( $t == '&' )
                    $t = '&#x27e8;';
                else if ( $t == '"' )
                    $t = '&#x301a;';
                return new editorial_text($t);
            case ']':
                $t = $this->lexval;
                $this->get_next();
                if ( $t == '&' )
                    $t = '&#x27e9;';
                else if ( $t == '"' )
                    $t = '&#x301b;';
                return new editorial_text($t);
                
            case '|': // overscript text
                $text = $this->lexval;
                $this->get_next();
                return new over_text($text,'l'); /// \todo allow other text alphabets?
            
            default:
                $this->error ( "parser: unexpected '{$this->la}'");                
                $this->get_next(); // skip unknown token
                return new spacer(0,0); // in case error doesn't throw
        }
    }
    
    private function enclosure()
    {
        $type = $this->lexval;
        $this->match('<');
        switch ( $type )
        {
            case '0':  
            case 'H0':
            case 'S0':                             // void 
                $start = new cap_void_start;            // void start
                $def_end = '0';                         // void end
                break;
            default:
            case '1':                               // cartouche            ( )|
                $start = new cap_smooth_start;          // smooth start
                $def_end = '2';                         // knot end
                break;
            case '2':                               // reverse cartouche    |( )
                $start = new cap_knot_start;            // knot start
                $def_end = '1';                         // smooth end
                break;
            case 'H':
            case 'H1':                              // hwt                  [ .]
                $start = new cap_square_start;          // square start
                $def_end = 'H2';                        // bottom square end
                break;
            case 'H2':                              // hwt                  [. ]
                $start = new cap_hwt_bottom_start;      // bottom square start
                $def_end = 'H1';                        // square end
                break;
            case 'H3':                              // hwt                  [' ]
                $start = new cap_hwt_top_start;         // top square start
                $def_end = 'H1';                        // square end
                break;
            case 'S':
            case 'S1':                              // serekh               [ ]=
                $start = new cap_square_start;          // square start
                $def_end = 'S2';                        // serekh end
                break;
            case 'S2':                              // reverse serekh       =[ ]
                $start = new cap_building_start;        // serekh start
                $def_end = 'H1';                        // square end
                break;
                
        }
        $content = $this->cadrat_base();
        $type = $this->lexval;
        $this->match('>');
        if ( $type == '' )
            $type = $def_end;
        switch ( $type )
        {
            case '0':
            case 'H0':
            case 'S0':
                $end = new cap_void_end;
                break;
            case '1':
                $end = new cap_smooth_end;
                break;
            default:
            case '2':                               
                $end = new cap_knot_end;
                break;
            case 'S1':
            case 'H1':
                $end = new cap_square_end;
                break;
            case 'H':
            case 'H2':
                $end = new cap_hwt_bottom_end;
                break;
            case 'H3':
                $end = new cap_hwt_top_end;
                break;
            case 'S':
            case 'S2':
                $end = new cap_building_end;
                break;
        }
        return new drawn_enclosure($start,$end,$content);
    }
    
    private function glyph()
    {
        if ( $this->la == '=' )
            $this->get_next();
        
        $code = $this->lexval;
        
        $this->match('GLYPH');
        
        if ( $this->la == '_' ) // both _ and __
            $this->get_next();
            
        if ( isset(globals::$special_glyphs[$code]) )
            return globals::$special_glyphs[$code];
            
        $code1 = get_code($code,2);
        if ( $code1 == null)
            return new text($code,'l'); // unknown glyph, render as text
            
        $glyph = new glyph($code1);
        $css_classes = '';
        while ( $this->la == '\\' )
        {
            $str = $this->lexval;
            $this->get_next();
            if ( strlen($str) == 0 )
                $glyph = new mirror($glyph);
            else if ( $str[0] == 'R' )
                $glyph = new rotate($glyph,(int)substr($str,1));
            else if ( ($str[0] == 'r' || $str[0] == 't') &&
                     strlen($str)>1 && ctype_digit($str[1]) )
            {
                $angle = 360 - 90 * (int)substr($str,1);
                $glyph = new rotate($glyph,$angle);
                if ( $str[0] == 't' )
                    $glyph = new mirror($glyph);
            }
            else if ( is_numeric($str) )
                $glyph = new scale($glyph,((double)$str)/100 );
            else // check for custom styling
            {
                $class = $this->styler->get_class($str);
                if ( $class != null )
                    $css_classes .= "$class ";
            }
            /*else if ( $str == 'red' )
                $glyph = new add_style($glyph,'red');
            else if ( $str == 'black' )
                $glyph = new add_style($glyph,'black');*/
            // else ignore
        }
        if ( strlen(trim($css_classes)) > 0 )
        {
            $glyph = new add_style($glyph,$css_classes);
        }
        
        return $glyph;
    }
    
    private function font_change()
    {
        $alphabet = $this->lexval;
        if ( $alphabet == '+' )
        {
            $string = $this->lex_text();
            $this->get_next();
            return new comment($string);
        }
        $bold = false;
        if ( $alphabet == 'b' )
        {
            $alphabet ='l';
            $bold = true;
        }
        $italic = false;
        if ( $alphabet == 'i' )
        {
            $alphabet ='l';
            $italic = true;
        }
        if ( !isset(text::$alphabets[$alphabet]) )
            $alphabet = 'l';
            
        $string = $this->lex_text();
        $this->get_next();
        
        $text = new text($string,$alphabet);
        if ( $bold )
            $text = new add_style($text, 'bold');
        else if ( $italic )
            $text = new add_style($text, 'italic');
            
        return $text;
    }
    
    /**
     * \brief Lexes non-Hieroglyphic text
     * \param bool $break_on_newline Whether it should stop on a newline
     */
    private function lex_text($break_on_newline = false)
    {
        $str = '';
        while ( !$this->eof )
        {
            $c = $this->get();
            if ( $c == '\\' )
            {
                $d = $this->get();
                if ( $this->eof )
                    break;
                if ( $d == '+' || $d == '\\' )
                    $str .= $d;
                else
                    $str .= $c.htmlspecialchars($d);
            }
            else if ( $c == '+' )
            {
                $this->lexval = $this->get();
                $this->la = '+';
                if ( $this->lexval != 's' )
                {
                    $this->error("parser: expected +s after font switch");
                    $this->recover[] = array('tok'=>'-'); // add separator and font switch on error
                    $this->recover[] = array('tok'=>'+','val'=>$this->lexval);
                }
                break;
            }
            else if ( $c == "\n" && $break_on_newline )
                break;
            else
                $str .= htmlspecialchars($c);
        }
        if ( $this->eof )
            $this->la = 'EOF';
        return $str;
    }
    
    private function lex()
    {
        if ( count($this->recover) != 0)
        {
            $tok = array_shift ( $this->recover );
            if ( isset($tok['val']) )
                $this->lexval = $tok['val'];
            return $tok['tok'];
        }
        
        $str = '';
        $c = null;
        do
            $c = $this->get();
        while ( !$this->eof && ord($c) <= ord(' ') ); // skip all non-printable|space ASCII characters
        
        if ( $this->eof )
            return 'EOF'; // end of file
        
        if ( $c == '-' )
        {
            $c = $this->get();
            if ( $c == '!' )
            {
                if ( $this->get() == '!' )
                    return '!!'; // end of page
                $this->unget();
                return '!'; // end of line
            }
            else if ( $c == '>' || ctype_alnum($c) )
            {
                $c = strtoupper($c);
                $got = 0;
                if ( $c == 'H' || $c == 'S' )
                {
                    $str = $c;
                    $c = $this->get();
                    $got++;
                }
                if ( $c == '0' || $c == '1' || $c == '2' || $c == '3' )
                {
                    $str .= $c;
                    $c = $this->get();
                    $got++;
                }
                if ( $c == '>' )
                {
                    $this->lexval = $str;
                    return '>'; // end cartouche
                }
                $this->unget($got);
                //return $this->lex();
            }
            elseif ( $c == '$' )
            {
                return $this->lex_color_change();
            }
            else if ( $c == '#' )
            {
                return $this->lex_color_change('b','e','#');
            }
            else if ( $c == '+')
            {
                $d = $this->get();
                if ( ctype_upper($d) )
                {
                    $this->lexval = $d;
                    return 'PRE'; // preformatted paragraph
                }
                $this->unget();
            }
            
            
            $this->unget();
            return '-'; // concatenation
        }
        else if ( $c == '#' )
        {
            $c = $this->get();
            if ( $c == '#' )
                return '##'; // overlay
            else if ( $c == 'b' || $c == 'e' )
            {
                if ( $this->get() != '-') // discard next -
                    $this->unget();
                return "#$c"; // long shading begin/end
            }
            
            while ( !$this->eof && ctype_digit($c) && (int)$c < 5)
            {
                $str .= $c;
                $c = $this->get();
            }
            $this->unget();
            if ( $str == '' )
                $str = '1234';
            $this->lexval = $str;
            return '#1234'; // partial shading
        }
        else if ( $c == ':' || $c == '*' ||
                  $c == '(' || $c == '=' || $c == '>')
        {
            return $c; // misc simple operators
        }
        else if ( $c == '&' || $c == ')' )
        {
            if ( $this->get() == ']' )
            {
                $this->lexval = "$c";
                return ']'; // closing editorial bracket
            }
            $this->unget();
            return $c;
        }
        else if ( $c == '\'' || $c == '"' || $c == '}' || $c == ']' )
        {
            if ( $this->get() != ']' )
            {
                $this->unget();
                $this->error("Expected '$c]', got just '$c'" );
            }
            $this->lexval = "$c";
            return ']'; // closing editorial bracket
        }
        else if ( $c == '[' )
        {
            $c = $this->get();
            if ( strpos('\'"{&([',$c) === false )
            {
                $this->unget();
                $this->error("Invalid editorial bracket");
                $c = '[';
            }
            $this->lexval = "$c";
            return '[';// open editorial bracket
        }
        else if ( $c == '<' )
        {
            while(true)
            {
                $c = strtoupper($this->get());
                if ( !ctype_alnum($c) )
                    break;
                $str .= $c;
            }
            if ( $c != '-' )
            {
                $this->error ( "lexer: expected '-' after cartouche start");
                $this->unget();
            }
            //$this->unget();
            $this->lexval = $str;
            return '<'; // start cartouche
        }
        else if (ctype_alnum($c))
        {
            if ( $c == 'v' || $c == 'h' )
            {
                if ( $this->get() == '/')
                {
                    $this->lexval = $c; // vertical|horizontal shade
                    return '/';
                }
                $this->unget();
            }
            
            do
            {
                $str .= $c;
                $c = $this->get();
            }while ( ctype_alnum($c) );
            
            /*if ( $c == '>' )
            {
                $this->lexval = strtoupper($str);
                return '>'; // end cartouche
            }*/
            $this->unget();
            //$c = $this->get();
            $this->lexval = $str;
            return 'GLYPH'; // Gard code, manuel
        }
        else if ( $c == '/' )
        {
            if ( $this->get() == '/' )
                $this->lexval = '/';
            else
            {
                $this->unget();
                $this->lexval = '';
            }
            return '/'; // big|small shade
        }
        else if ( $c == '.' )
        {
            $this->lexval = .5;
            if ( $this->get() == '.' )
                $this->lexval = 1;
            else
                $this->unget();
            return '.'; // small|large space
        }
        else if ( $c == '?' )
        {
            do
            {
                $c = $this->get();
                $str .= $c;
            }while ( ctype_alnum($c) );
            $this->unget();
            $this->lexval = -(int)substr($str,0,-1) / 100;
            return '.'; // tabs
        }
        else if ( $c == '|' )
        {
            do
            {
                $c = $this->get();
                if ( $c == '\\')
                {
                    $d = $this->get();
                    if ( $this->eof )
                        break;
                    else if ( $d == '-' || $d == '\\' )
                        $str .= $d;
                    else
                        $str .= $c.htmlspecialchars($d);
                }
                $str .= htmlspecialchars($c);
            }while ( !$this->eof && $c != '-' );
            $this->unget();
            $this->lexval = substr ( $str, 0, -1 );
            return '|'; // overscript
        }
        else if ( $c == '_' )
        {
            if ( $this->get() != '_' )
                $this->unget();
            return '_'; // word/prase grammar ending
        }
        else if ( $c == '\\' )
        {
            do
            {
                $c = $this->get();
                $str .= $c;
            }while ( ctype_alnum($c) );
            $this->unget();
            $this->lexval = substr($str,0,-1);
            return '\\'; // sign modifiers
        }
        else if ( $c == '+' )
        {
            $c = $this->get();
            $this->lexval = $c;
            if ( ctype_upper($c) )
                return 'PRE'; // preformatted paragraph
            else
                return '+'; // font change
        }
        else if ( $c == '!' ) // allow -! and -!! as ! and !! ( w/o - )
        {
            if ( $this->get() == '!' )
                return '!!'; // end of page
            $this->unget();
            return '!'; // end of line
        }
        else if ( $c == '$' )
            return $this->lex_color_change();
        
        $this->error("lexer: unknown token '$c'");
        return $this->lex(); // keep reading
    }
    
    private function lex_color_change($begin='b',$end='r',$mark='$')
    {
        $c = $this->get();
        if ( $c == $begin || $c == $end )
        {
            if ( $this->get() != '-') // discard next -
                $this->unget();
            return "$mark$c"; // red/black color switch
        }
        else
        {
            $this->unget();
            $this->error("lexer: expected {$mark}b or {$mark}r, got {$mark}$c");
            return $this->lex(); // lex next token
        }
    }
}

?>