<?php

namespace svglyph;

abstract class modifier extends cadrat
{
    public $cadrat; ///< Modified object
    
    function __construct($cadrat)
    {
        $this->cadrat = $cadrat;
    }
    
    function height($height)
    {
        return $this->cadrat->height($height);
    }
    
    function width($height)
    {
        return $this->cadrat->width($height);
    }
}

/// Shades some of the contained cadrat quarters
class partial_shade extends modifier
{
    public $quarters; ///< String with values 1234 for local shading
    
    function __construct($cadrat, $quarters)
    {
        $this->quarters = $quarters;
        parent::__construct($cadrat);
    }
    
    function render($x, $y, $height)
    {
        $rendering = '<g>';
        
        $rendering .= $this->cadrat->render($x, $y, $height);
        
        $cad_h = $this->cadrat->height($height);
        
        $w2 = $this->cadrat->width($height)/2;
        $h2 = $cad_h/2;
        
        if ( $cad_h < $height ) // center shading
        {
            $y += ($height-$cad_h)/2;
        }
        
        //top left
        if ( strpos($this->quarters,'1') !== FALSE )
            $rendering .= "<rect class='shade' x='$x' y='$y' width='$w2' height='$h2' />";
        // top right
        if ( strpos($this->quarters,'2') !== FALSE )
            $rendering .= "<rect class='shade' x='".($x+$w2)."' y='$y' width='$w2' height='$h2' />";
        // bottom left
        if ( strpos($this->quarters,'3') !== FALSE )
            $rendering .= "<rect class='shade' x='$x' y='".($y+$h2)."' width='$w2' height='$h2' />";
        // top right
        if ( strpos($this->quarters,'4') !== FALSE )
            $rendering .= "<rect class='shade' x='".($x+$w2)."' y='".($y+$h2)."' width='$w2' height='$h2' />";
            
        return "$rendering</g>";
    }
    
}

/// Adds custom style class
class add_style extends modifier
{
    public $css_class;
    
    function __construct($cadrat, $css_class)
    {
        $this->css_class = $css_class;
        parent::__construct($cadrat);
    }
    
    
    function render($x, $y, $height)
    {
        return "<g class='{$this->css_class}'>".
                    $this->cadrat->render($x,$y,$height).
                '</g>';
    }
}

/// Mirrors the sign (horizontally)
class mirror extends modifier
{
    function __construct ( $cadrat )
    {
        parent::__construct($cadrat);
    }
    
    function render($x,$y,$height)
    {
        $w = $this->cadrat->width($height);
        
        $rendering = '<g transform="scale(-1,1)translate('.(-2*$x-$w).',0)" >';
        
        $rendering .= $this->cadrat->render($x,$y,$height);
            
        return "$rendering</g>";
    }
}

/// Rotates sign by angle and scales to fit line height
class rotate extends modifier
{
    public $angle; ///< Degrees
    
    function __construct ( $cadrat, $angle )
    {
        $this->angle = $angle;
        parent::__construct($cadrat);
    }
    
    /* /// \return the width of the rotated bounding box for the contained cadrat
    function unscaled_width($height)
    {
        list($w,$h)=$this->bounding_box($height,$this->angle);
        return $w;
    }
    
    /// \return the height of the rotated bounding box for the contained cadrat
    function unscaled_height($height)
    {
        list($w,$h)=$this->bounding_box($height,$this->angle);
        return $w;
    }*/
    
    /// \return scale factor given available height and eventual computed unscaled height
    private function optimized_scale_factor($height,$unscaled_height)
    {
        if ( $unscaled_height == null )
            list($null,$unscaled_height )=$this->cadrat->bounding_box($height,$this->angle);
        
        $factor = min ( 1, $height/$unscaled_height );
        
        /*if ( globals::$optimal_rotation )
            return $factor;*/
        
        return ( 1 + $factor ) / 2;
        /**
            \note
            min ( 1, $height/$this->unscaled_height($height) ) )
                would scale regarding the rotated bounding box,
                the maximum size unless globals::$optimal_rotation is true
            1 (no scaling)
                would 'scale' regarding the circle inscribed in the bounding box,
                the minimum size.
            Averaging the two should result in more accurate/general results
            For a perfect factor the Path data coordinates should be taken in account
            re-evaluating the bounding box for the rotated path, this occurs only
            for glyphs and only if globals::$optimal_rotation is true
        */
    }
    function scale_factor($height)
    {
        return $this->optimized_scale_factor($height,null);
    }
    
    function width($height)
    {
        list($w,$h) = $this->cadrat->bounding_box($height,$this->angle);
        //echo "(H=$height,w=$w,h=$h,s=".$this->optimized_scale_factor($height,$h).")\n";
        return $w * $this->optimized_scale_factor($height,$h);
    }
    
    function height($height)
    {
        list($w,$h) = $this->cadrat->bounding_box($height,$this->angle);
        return $h * $this->optimized_scale_factor($height,$h);
    }
    
    function render($x, $y, $height)
    {
        $glyphw = $this->cadrat->width($height);
        $glyphh = $this->cadrat->height($height);

        $cy = $height/2;
        $cx = $glyphw/2;
        
        $scale_factor = $this->scale_factor($height);
        
        $dx = $x+($this->width($height)-$glyphw)/2; // $x+ to avoid local glyph transformation
        $dy = $y;//+($this->height($height)-$glyphh)/2;
        
        /*if ( $scale_factor < 1)
        { // compensate movement caused by coord transformation
            $dx += ($this->width($height)-$glyphw)*(1-$scale_factor);   
            $dy -= 0;// ($this->height($height)-$glyphh)*($scale_factor);
        }*/
        
        /*echo "<!--1\n".
            "scale=$scale_factor".
            "\ndx=$dx".
            "\ndy=$dy".
            "\ncompw=".$this->width($height).
            "\ncomph=".$this->height($height).
            "\nglyphw=$glyphw".
            "\nyc=$cy".
            "\nxc=$cx".
            "\nah=$height".
            "\n-->";*/
        
        //$rendering = "<circle cx='".($dx+$cx)."' cy='".($dy+$cy)."' r='5' />";
        
        $rendering = "<g transform='".
                        "translate($dx,$dy)".
                        "scale($scale_factor)".
                        "translate(".
                            ($cx*(1-$scale_factor)).",".
                            ($cy*(1-$scale_factor)).")".
                        "rotate({$this->angle},$cx,$cy)".
                        "translate(".-$x.",".-$y.")". // remove transform found in glyph::render()
                        "' >"; /// \todo use matrix
                        
        $rendering .= $this->cadrat->render($x,$y,$height);
            
        return "$rendering</g>";
    }
}

/// Scales up to the line height
class scale extends modifier
{
    public $factor;
    
    function __construct ( $cadrat, $factor )
    {
        $this->factor = $factor;
        parent::__construct($cadrat);
    }
    
    function height ( $height )
    {
        $ch = $this->cadrat->height($height);
        $max_factor = $height / $ch;
        return min ( $this->factor, $max_factor ) * $ch;
    }
    
    function width ( $height )
    {
        return $this->cadrat->width ( $height ) * $this->actual_factor($height);
    }
    
    function actual_factor ( $height )
    {
        $max_factor = $height / $this->cadrat->height($height);
        return min ( $this->factor, $max_factor );
    }
    
    function render ( $x, $y, $height )
    {
        $actual_factor = $this->actual_factor($height);
        
        //echo "<!--\nf={$this->factor}\na=$actual_factor\n-->";
        return "<g transform='".
                        "translate($x,".($y+$height/2).")".
                        "scale($actual_factor)".
                        "translate(".-$x.",".(-$y-$height/2).")".
                "'>".
                    $this->cadrat->render($x,$y, $height ).
                "</g>";
    }
    
}

?>